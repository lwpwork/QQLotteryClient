package com.bsmyl.lottery;

import cn.hutool.core.date.DateUtil;
import com.bsmyl.lottery.Service.AnalysisDataAble;
import com.bsmyl.lottery.Service.ImportLotteryData;
import com.bsmyl.lottery.dao.QQLotteryDao;
import com.bsmyl.lottery.model.QQLottery;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * describe:
 *
 * @author LiuWenPing
 * @date 2019/02/18
 */

//@RunWith(SpringRunner.class)
//@SpringBootTest
public class MySQLTest {

    //@Test
    public void test() {
        System.out.println("这是一个测试");
    }


    @Autowired
    private QQLotteryDao qqLotteryDao;


    @Autowired
    private ImportLotteryData<QQLottery> qqImportLotteryData;

  /*  @Test
    public void mySqlConlactionTest() {

        String fileName = "C:\\Users\\Administrator\\Desktop\\1.txt";
        List<QQLottery> qqLotteries = qqImportLotteryData.importData(fileName, new AnalysisDataAble() {
            @Override
            public Object dataAnalysis(String data) {
                {
                    String[] sourceStrArray = data.split("\t");
                    QQLottery qqLottery = new QQLottery();
                    String[] qqLotteryStr = new String[7];
                    int k = 0;
                    for (int i = 0; i < sourceStrArray.length; i++) {
                        if (sourceStrArray[i] != null && !sourceStrArray[i].equals("")) {
                            qqLotteryStr[k] = sourceStrArray[i];
                            k++;
                        }
                    }
                    for (int i = 0; i < qqLotteryStr.length; i++) {
                        switch (i) {
                            case 0:
                                String dateStr = qqLotteryStr[i];
                                StringBuffer dateStrbuf = new StringBuffer();
                                //1、初始化十分
                                String[] dateStrs = dateStr.split(" ");
                                if (dateStrs[1].length() == 4) {
                                    dateStrs[1] = "0"+dateStrs[1];
                                }
                                //2、初始化日期
                                String[] dates = dateStrs[0].split("/");
                                StringBuffer sb = new StringBuffer();
                                for (int j = 0; j < dates.length; j++) {

                                    if (dates[j].length() == 1) {
                                        dates[j] = "0" + dates[j];
                                        sb.append("-"+dates[j]);
                                    } else {
                                        if (j != 0) {
                                            sb.append("-");
                                        }
                                        sb.append(dates[j]);
                                    }
                                }
                                dateStrbuf.append(sb.toString()+" ");
                                dateStrbuf.append(dateStrs[1]);
                                qqLotteryStr[i] = dateStrbuf.toString();
                                Date date = DateUtil.parse(qqLotteryStr[i]);
                                qqLottery.setNumTime(date);
                                break;
                            case 1:
                                qqLottery.setOne(Integer.valueOf(qqLotteryStr[i]));
                                break;
                            case 2:
                                qqLottery.setTwo(Integer.valueOf(qqLotteryStr[i]));
                                break;
                            case 3:
                                qqLottery.setThree(Integer.valueOf(qqLotteryStr[i]));
                                break;
                            case 4:
                                qqLottery.setFour(Integer.valueOf(qqLotteryStr[i]));
                                break;
                            case 5:
                                qqLottery.setFive(Integer.valueOf(qqLotteryStr[i]));
                                break;
                            case 6:
                                qqLottery.setNum(Long.valueOf(qqLotteryStr[i]));
                                break;
                        }
                    }
                    return qqLottery;
                }
            }
        });
        List<QQLottery> tempList = new ArrayList<>();
        double size = 5000d;
        double pageNum = Math.ceil(qqLotteries.size() / size);
        for (int i = 0; i < pageNum; i++) {
            if (i + 1 < pageNum) {
                tempList = qqLotteries.subList(i * (int) size, (i + 1) * (int) size);
            } else {
                tempList = qqLotteries.subList(i * (int) size, qqLotteries.size());
            }
            qqLotteryDao.insertList(tempList);
        }
        //qqLotteryDao.insertList(qqLotteries);

    }*/



}
