package com.bsmyl.lottery;

import com.alibaba.fastjson.JSON;
import com.bsmyl.lottery.Service.BetPlan;
import com.bsmyl.lottery.Service.IssueService;
import com.bsmyl.lottery.Service.QqLotteryService;
import com.bsmyl.lottery.config.FeignBasicAuthRequestInterceptor;
import com.bsmyl.lottery.core.MapUtile;
import com.bsmyl.lottery.dao.LotteryDao;
import com.bsmyl.lottery.dao.QQLotteryDao;
import com.bsmyl.lottery.feign.LotteryFeignClient;
import com.bsmyl.lottery.feign.NewFieldLotteryClient;
import com.bsmyl.lottery.model.QQLottery;
import com.bsmyl.lottery.newfield.QqLotteryConfig;
import com.bsmyl.lottery.newfield.QqLotteryVO;
import com.bsmyl.lottery.newfield.bet.QqLotteryBet;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LotteryApplicationTests {

    //@Test
    public void contextLoads() {
        System.out.println("这是测试");
    }


    @Autowired
    private LotteryFeignClient lotteryFeignClient;

    @Autowired
    private BetPlan betPlan;

    @Autowired
    private LotteryDao lotteryDao;

    @Autowired
    private IssueService issueService;

    @Autowired
    private NewFieldLotteryClient newFieldLotteryClient;

    @Autowired
    private QqLotteryBet qqLotteryBet;

    //@Test
    public void LotteryHistoryOpenInfo(){
        issueService.initLotteryInfo();
    }

    @Autowired
    private QQLotteryDao qqLotteryDao;

    @Autowired
    private QqLotteryService qqLotteryService;

    @Test
    public void test(){
        //List<QQLottery> qqLotteries = qqLotteryDao.qqLotteryList();simulationBet
        qqLotteryService.simulationdLhBet();
        System.out.println("查询结束");
    }











}

