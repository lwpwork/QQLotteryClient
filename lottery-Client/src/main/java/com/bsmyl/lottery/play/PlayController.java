package com.bsmyl.lottery.play;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.bsmyl.lottery.Service.*;
import com.bsmyl.lottery.config.LogConfig;
import com.bsmyl.lottery.config.LotteryConfig;
import com.bsmyl.lottery.feign.LotteryFeignClient;
import com.bsmyl.lottery.model.*;
import com.bsmyl.lottery.newfield.bet.QqLotteryBet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * describe:
 *
 * @author LiuWenPing
 * @date 2019/02/15
 */

@Component
@Configurable
@EnableScheduling
public class PlayController {


    private static final Logger LOG = LoggerFactory.getLogger(PlayController.class);

    @Autowired
    private LotteryFeignClient lotteryFeignClient;

    @Autowired
    private BetPlan betPlan;

    @Autowired
    private IssueService issueService;

    @Autowired
    private PlayService playService;

    @Autowired
    private QqLotteryBet qqLotteryBet;

    @Autowired
    private PlanService planService;

    @Autowired
    private SimulationPlanService simulationPlanService;

    private static Lottery prevLottery ;

//    @Scheduled(cron = "*/1 * * * * *")
//    public void task() {
//        try {
//            LotteryHistoryOpenInfo();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }


    @Scheduled(cron = "*/5 * * * * *")
    public void PkInitTask(){
        planService.plan();
        simulationPlanService.qqLotterySimulationPlan();
    }



//    @Scheduled(cron = "*/1 * * * * *")
//    public void autoPkBet(){
//        playService.pk10TowMinuteBet();
//    }


//    @Scheduled(cron = "*/5 * * * * *")
//    public void qqLotteryAutoBet(){
//        try {
//            qqLotteryBet.listenQqLottery();
//        } catch (Exception e) {
//            qqLotteryBet.listenQqLottery();
//            e.printStackTrace();
//        }
//    }



    public void LotteryHistoryOpenInfo() throws InterruptedException {
        LotteryInfoParam lotteryInfoParam = null;
        if (LotteryConfig.Token != null && !LotteryConfig.Token.equals("")) {
            lotteryInfoParam = new LotteryInfoParam(LotteryConfig.Token,1025, 4);
        }


        if (lotteryInfoParam == null) {
            //LOG.info("彩票自动押注未工作");
            return;
        }
        Result res = lotteryFeignClient.LotteryHistoryOpenInfo(JSON.toJSONString(lotteryInfoParam));

        //验证是否是最新记录：最新记录进行计划生成并押注。

        initLotteryContinuousPlan(res);
        /*if (prevLottery != null) {
            Long issue = res.getData().getHistoryList().get(0).getIssue();
            if (issue.longValue() != prevLottery.getIssue().longValue()) {
                prevLottery = res.getData().getHistoryList().get(0);
                List<BetPlanModel> betPlanModels = betPlan.genBetPlans(res.getData().getHistoryList());
                LOG.info(JSON.toJSONString(res));
                LOG.info(JSON.toJSONString(betPlanModels));
                LotteryUserBet(betPlanModels,res);
            }
        } else {
            List<BetPlanModel> betPlanModels = betPlan.genBetPlans(res.getData().getHistoryList());
            prevLottery = res.getData().getHistoryList().get(0);
            LOG.info(JSON.toJSONString(res));
            LOG.info(JSON.toJSONString(betPlanModels));
            LotteryUserBet(betPlanModels,res);
        }*/

    }

    public void initLotteryContinuousPlan(Result res){
        if (prevLottery != null) {
            Long issue = res.getData().getHistoryList().get(0).getIssue();
            if (issue.longValue() != prevLottery.getIssue().longValue()) {
                prevLottery = res.getData().getHistoryList().get(0);
                List<BetPlanModel> betPlanModels = betPlan.getContinuousPlan(res.getData().getHistoryList());
                LOG.info(JSON.toJSONString(res));
                LOG.info(JSON.toJSONString(betPlanModels));
                LotteryUserBet(betPlanModels,res);
            }
        } else {
            List<BetPlanModel> betPlanModels = betPlan.getContinuousPlan(res.getData().getHistoryList());
            prevLottery = res.getData().getHistoryList().get(0);
            LOG.info(JSON.toJSONString(res));
            LOG.info(JSON.toJSONString(betPlanModels));
            LotteryUserBet(betPlanModels,res);
        }
    }

    public void LotteryUserBet(List<BetPlanModel> betPlanModels,Result result){
        List<Bet> betList = new ArrayList<>();
        int i = 0;
        Long playId = 101151010l;
        //初始化betList
        for(BetPlanModel betPlanModel :
                betPlanModels) {
            StringBuffer bet = new StringBuffer();
            if (betPlanModel.getbOrS() != null && betPlanModel.getbOrS().length > 0) {
                for (int j = 0; j < 5; j++) {
                    bet.append(betPlanModel.getbOrS()[j] + " ");
                }
                String betStr = bet.toString().trim();
                String buyCodeFront = buyCodeFront(betStr, i);
                String buyCode = buyCode(betStr.replace(" ", ""), i);
                Bet bet1 = new Bet(playId,buyCode,buyCodeFront,200,1);
                betList.add(bet1);
            }
            if (betPlanModel.getsOrD() != null && betPlanModel.getsOrD().length > 0) {
                bet = new StringBuffer();
                for (int j = 0; j < 5; j++) {
                    bet.append(betPlanModel.getsOrD()[j] + " ");
                }
                String betStr = bet.toString().trim();
                String buyCodeFront = buyCodeFront(betStr, i);
                String buyCode = buyCode(betStr.replace(" ", ""), i);
                Bet bet1 = new Bet(playId,buyCode,buyCodeFront,200,1);
                betList.add(bet1);
            }
            i++;
        }
        Long issue = result.getData().getHistoryList().get(0).getIssue()+1;
        int randomNum = (int)((Math.random()*9+1)*1000);
        Order order = new Order("LotteryUserBet",1025,issue.toString(),JSON.toJSONString(betList),String.valueOf(System.currentTimeMillis()+randomNum)+randomNum,LotteryConfig.Token);
        LOG.info(JSON.toJSONString(order));
        Result res = lotteryFeignClient.LotteryUserBet(order);
        LOG.info(JSON.toJSONString(res));
    }

    public String buyCodeFront(String bet,int index){
        StringBuffer stringBuffer = new StringBuffer("-,-,-,-,-");
        stringBuffer.replace(2*index, 2 * index+1, bet);
        return stringBuffer.toString();
    }

    public String buyCode(String bet,int index){
        StringBuffer stringBuffer = new StringBuffer(",,,,,");
        stringBuffer.replace(index, index+1, bet);
        return stringBuffer.toString();
    }



}
