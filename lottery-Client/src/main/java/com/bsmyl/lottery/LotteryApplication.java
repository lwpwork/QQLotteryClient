package com.bsmyl.lottery;

import com.bsmyl.lottery.core.MapUtile;
import com.bsmyl.lottery.newfield.QqLotteryConfig;
import com.bsmyl.lottery.newfield.bet.QqLotteryBet;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableFeignClients
//@EnableAutoConfiguration
public class LotteryApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(LotteryApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(LotteryApplication.class);
    }

    @Bean
    public void initLotteryConfig() throws Exception {
        QqLotteryConfig qqLotteryConfig = new QqLotteryConfig();
        QqLotteryBet.lotteryConfig = MapUtile.objectToMap(qqLotteryConfig);
    }


}

