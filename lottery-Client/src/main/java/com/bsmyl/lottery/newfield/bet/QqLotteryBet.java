package com.bsmyl.lottery.newfield.bet;

import com.alibaba.fastjson.JSON;
import com.bsmyl.lottery.config.FeignBasicAuthRequestInterceptor;
import com.bsmyl.lottery.dao.QQLotteryDao;
import com.bsmyl.lottery.feign.NewFieldLotteryClient;
import com.bsmyl.lottery.model.Lottery;
import com.bsmyl.lottery.model.QQLottery;
import com.bsmyl.lottery.newfield.QqLotteryVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.naming.Name;
import javax.swing.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * describe:
 *
 * @author LiuWenPing
 * @date 2019/02/27
 */
@Component
public class QqLotteryBet {

    public static Map<String, Object> lotteryConfig;

    public static Map<String, Object> lotteryConfig02;

    @Autowired
    private NewFieldLotteryClient newFieldLotteryClient;

    QQLottery prveQqLottery;

    Map<String,Object> prveLotteryModel = new HashMap<>();

    @Autowired
    private QQLotteryDao qqLotteryDao;

    public void listenQqLottery() {
        if (FeignBasicAuthRequestInterceptor.sessionID == null || FeignBasicAuthRequestInterceptor.sessionID.equals("")) {
            return;
        }
        Map<String, Object> map = (Map<String, Object>) newFieldLotteryClient.loopGameLottery("qqmin").get("data");
        if (map == null) {
            return;
        }
        Map<String,Object> lotteryModel = (Map<String,Object>) map.get("gameOpenCode");
        if (!lotteryModel.get("issue").equals(prveLotteryModel.get("issue"))) {
            bet();
            prveLotteryModel = lotteryModel;
        }
    }

    /**
     * 押注：前三、中三、后三、定位胆、龙虎
     */
    private void bet() {
        List<QqLotteryVO> bets = new ArrayList<>();
        if (lotteryConfig != null) {
            for (String key :
                    lotteryConfig.keySet()) {
                if (!key.equals("model") && !key.equals("multiple")) {
                    if (lotteryConfig.get(key) != null) {
                        QqLotteryVO qqLotteryVO = new QqLotteryVO();
                        qqLotteryVO.setMethod(key);
                        qqLotteryVO.setContent(lotteryConfig.get(key).toString());
                        qqLotteryVO.setModel(lotteryConfig.get("model").toString());
                        qqLotteryVO.setMultiple(Integer.valueOf(lotteryConfig.get("multiple").toString()));
                        bets.add(qqLotteryVO);
                    }
                }
            }
        }
        if (lotteryConfig02 != null) {
            for (String key :
                    lotteryConfig02.keySet()) {
                if (!key.equals("model") && !key.equals("multiple")) {
                    if (lotteryConfig.get(key) != null) {
                        QqLotteryVO qqLotteryVO = new QqLotteryVO();
                        qqLotteryVO.setMethod(key);
                        qqLotteryVO.setContent(lotteryConfig02.get(key).toString());
                        qqLotteryVO.setModel(lotteryConfig02.get("model").toString());
                        qqLotteryVO.setMultiple(Integer.valueOf(lotteryConfig02.get("multiple").toString()));
                        bets.add(qqLotteryVO);
                    }
                }
            }
        }
        String jsonStr = JSON.toJSONString(bets);
        System.out.println(jsonStr);
        Map<String,Object> map = newFieldLotteryClient.addOrder(jsonStr);
        if (map.get("message").toString().equals("您还没有登录")) {
            FeignBasicAuthRequestInterceptor.sessionID = null;
        }
        if (map.get("error").equals(1)) {
            return;
        }
        System.out.println(JSON.toJSONString(map));
    }


}
