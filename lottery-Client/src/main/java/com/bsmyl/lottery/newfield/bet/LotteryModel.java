package com.bsmyl.lottery.newfield.bet;

import java.util.Date;

/**
 * describe:
 *
 * @author LiuWenPing
 * @date 2019/03/04
 */
public class LotteryModel {
    private Long id;
    private Integer clearStatus;
    private Date clearTime;
    private String code;
    private String code1;
    private String code2;
    private String issue;
    private String lottery;
    private Date openTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getClearStatus() {
        return clearStatus;
    }

    public void setClearStatus(Integer clearStatus) {
        this.clearStatus = clearStatus;
    }

    public Date getClearTime() {
        return clearTime;
    }

    public void setClearTime(Date clearTime) {
        this.clearTime = clearTime;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode1() {
        return code1;
    }

    public void setCode1(String code1) {
        this.code1 = code1;
    }

    public String getCode2() {
        return code2;
    }

    public void setCode2(String code2) {
        this.code2 = code2;
    }

    public String getIssue() {
        return issue.replace("-","");
    }

    public void setIssue(String issue) {
        this.issue = issue;
    }

    public String getLottery() {
        return lottery;
    }

    public void setLottery(String lottery) {
        this.lottery = lottery;
    }

    public Date getOpenTime() {
        return openTime;
    }

    public void setOpenTime(Date openTime) {
        this.openTime = openTime;
    }



}
