package com.bsmyl.lottery.newfield;

/**
 * describe:
 *
 * @author LiuWenPing
 * @date 2019/02/27
 */
public class QqLotteryVO {
    private String lottery = "qqmin";//彩种
    private String issue = "";
    private String method;  //押注方式
    private String content; //押注内容
    private String model;      //单位
    private Integer multiple;  //倍率
    private Integer code = 1990;
    private boolean compress = false;

    public QqLotteryVO(String method, String content, String model, Integer multiple) {
        this.method = method;
        this.content = content;
        this.model = model;
        this.multiple = multiple;
    }

    public QqLotteryVO() {
    }

    public String getLottery() {
        return lottery;
    }

    public void setLottery(String lottery) {
        this.lottery = lottery;
    }

    public String getIssue() {
        return issue;
    }

    public void setIssue(String issue) {
        this.issue = issue;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getMultiple() {
        return multiple;
    }

    public void setMultiple(Integer multiple) {
        this.multiple = multiple;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public boolean isCompress() {
        return compress;
    }

    public void setCompress(boolean compress) {
        this.compress = compress;
    }
}
