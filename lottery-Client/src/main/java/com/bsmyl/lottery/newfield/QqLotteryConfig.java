package com.bsmyl.lottery.newfield;

/**
 * describe:
 *
 * @author LiuWenPing
 * @date 2019/02/27
 */
public class QqLotteryConfig {
    private String sxzhixfsq;   //1-大；2-小；3-单；4-双 ,5-大单, 6-小双；
    private String sxzhixfsz;   //1-大；2-小；3-单；4-双 ,5-大单, 6-小双；
    private String sxzhixfsh;   //1-大；2-小；3-单；4-双 ,5-大单, 6-小双；
    private String dw ;  //1-大；2-小；3-单；4-双 ,5-大单, 6-小双；；
    private String lhwq;   //1-龙；2-虎；3-和；
    private String lhwb;   //1-龙；2-虎；3-和；
    private String lhws;   //1-龙；2-虎；3-和；
    private String lhwg;   //1-龙；2-虎；3-和；
    private String lhqb;   //1-龙；2-虎；3-和；
    private String lhqs;   //1-龙；2-虎；3-和；
    private String lhqg;   //1-龙；2-虎；3-和；
    private String lhbs;   //1-龙；2-虎；3-和；
    private String lhbg;   //1-龙；2-虎；3-和；
    private String lhsg;   //1-龙；2-虎；3-和；

    private String model = "li"; //钱的单位
    private Integer multiple = 1; //押注倍数


    public String getSxzhixfsq() {
        return sxzhixfsq;
    }

    public void setSxzhixfsq(String sxzhixfsq) {
        if ("1".equals(sxzhixfsq)) {
            this.sxzhixfsq = "56789,56789,56789,-,-";
        } else if ("2".equals(sxzhixfsq)) {
            this.sxzhixfsq = "01234,01234,01234,-,-";
        } else if ("3".equals(sxzhixfsq)) {
            this.sxzhixfsq = "13579,13579,13579,-,-";
        } else if ("4".equals(sxzhixfsq)) {
            this.sxzhixfsq = "02468,02468,02468,-,-";
        } else {
            this.sxzhixfsq = null;
        }

    }

    public String getSxzhixfsz() {
        return sxzhixfsz;
    }

    public void setSxzhixfsz(String sxzhixfsz) {
        if ("1".equals(sxzhixfsz)) {
            this.sxzhixfsz = "-,56789,56789,56789,-";
        } else if ("2".equals(sxzhixfsz)) {
            this.sxzhixfsz = "-,01234,01234,01234,-";
        } else if ("3".equals(sxzhixfsz)) {
            this.sxzhixfsz = "-,13579,13579,13579,-";
        } else if ("4".equals(sxzhixfsz)) {
            this.sxzhixfsz = "-,02468,02468,02468,-";
        } else {
            this.sxzhixfsz = null;
        }
    }

    public String getSxzhixfsh() {
        return sxzhixfsh;
    }

    public void setSxzhixfsh(String sxzhixfsh) {
        if ("1".equals(sxzhixfsh)) {
            this.sxzhixfsh = "-,-,56789,56789,56789";
        } else if ("2".equals(sxzhixfsh)) {
            this.sxzhixfsh = "-,-,01234,01234,01234";
        } else if ("3".equals(sxzhixfsh)) {
            this.sxzhixfsh = "-,-,13579,13579,13579";
        } else if ("4".equals(sxzhixfsh)) {
            this.sxzhixfsh = "-,-,02468,02468,02468";
        } else {
            this.sxzhixfsh = null;
        }
    }

    public String getDw() {
        return dw;
    }

    public void setDw(String dw) {
        if ("1".equals(dw)) {
            this.dw = "56789,56789,56789,56789,56789";
        } else if ("2".equals(dw)) {
            this.dw = "01234,01234,01234,01234,01234";
        } else if ("3".equals(dw)) {
            this.dw = "13579,13579,13579,13579,13579";
        } else if ("4".equals(dw)) {
            this.dw = "02468,02468,02468,02468,02468";
        } else {
            this.dw = null;
        }
    }

    public String getLhwq() {
        return lhwq;
    }

    public void setLhwq(String lhwq) {
        if ("1".equals(lhwq)) {//龙
            this.lhwq = "龙";
        } else if ("2".equals(lhwq)) {//虎
            this.lhwq = "虎";
        } else if ("3".equals(lhwq)) {//和
            this.lhwq = "和";
        }  else {
            this.lhwq = null;
        }
    }

    public String getLhwb() {
        return lhwb;
    }

    public void setLhwb(String lhwb) {
        if ("1".equals(lhwb)) {//龙
            this.lhwb = "龙";
        } else if ("2".equals(lhwb)) {//虎
            this.lhwb = "虎";
        } else if ("3".equals(lhwb)) {//和
            this.lhwb = "和";
        } else {
            this.lhwb = null;
        }
    }

    public String getLhws() {
        return lhws;
    }

    public void setLhws(String lhws) {
        if ("1".equals(lhws)) {//龙
            this.lhws = "龙";
        } else if ("2".equals(lhws)) {//虎
            this.lhws = "虎";
        } else if ("3".equals(lhws)) {//和
            this.lhws = "和";
        } else {
            this.lhws = null;
        }
    }

    public String getLhwg() {
        return lhwg;
    }

    public void setLhwg(String lhwg) {
        if ("1".equals(lhwg)) {//龙
            this.lhwg = "龙";
        } else if ("2".equals(lhwg)) {//虎
            this.lhwg = "虎";
        } else if ("3".equals(lhwg)) {//和
            this.lhwg = "和";
        } else {
            this.lhwg = null;
        }
    }

    public String getLhqb() {
        return lhqb;
    }

    public void setLhqb(String lhqb) {
        if ("1".equals(lhqb)) {//龙
            this.lhqb = "龙";
        } else if ("2".equals(lhqb)) {//虎
            this.lhqb = "虎";
        } else if ("3".equals(lhqb)) {//和
            this.lhqb = "和";
        } else {
            this.lhqb = null;
        }
    }

    public String getLhqs() {
        return lhqs;
    }

    public void setLhqs(String lhqs) {
        if ("1".equals(lhqs)) {//龙
            this.lhqs = "龙";
        } else if ("2".equals(lhqs)) {//虎
            this.lhqs = "虎";
        } else if ("3".equals(lhqs)) {//和
            this.lhqs = "和";
        } else {
            this.lhqs = null;
        }
    }

    public String getLhqg() {
        return lhqg;
    }

    public void setLhqg(String lhqg) {
        if ("1".equals(lhqg)) {//龙
            this.lhqg = "龙";
        } else if ("2".equals(lhqg)) {//虎
            this.lhqg = "虎";
        } else if ("3".equals(lhqg)) {//和
            this.lhqg = "和";
        } else {
            this.lhqg = null;
        }
    }

    public String getLhbs() {
        return lhbs;
    }

    public void setLhbs(String lhbs) {
        if ("1".equals(lhbs)) {//龙
            this.lhbs = "龙";
        } else if ("2".equals(lhbs)) {//虎
            this.lhbs = "虎";
        } else if ("3".equals(lhbs)) {//和
            this.lhbs = "和";
        } else {
            this.lhbs = null;
        }
    }

    public String getLhbg() {
        return lhbg;
    }

    public void setLhbg(String lhbg) {
        if ("1".equals(lhbg)) {//龙
            this.lhbg = "龙";
        } else if ("2".equals(lhbg)) {//虎
            this.lhbg = "虎";
        } else if ("3".equals(lhbg)) {//和
            this.lhbg = "和";
        } else {
            this.lhbg = null;
        }
    }

    public String getLhsg() {
        return lhsg;
    }

    public void setLhsg(String lhsg) {
        if ("1".equals(lhsg)) {//龙
            this.lhsg = "龙";
        } else if ("2".equals(lhsg)) {//虎
            this.lhsg = "虎";
        } else if ("3".equals(lhsg)) {//和
            this.lhsg = "和";
        } else {
            this.lhsg = null;
        }
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getMultiple() {
        return multiple;
    }

    public void setMultiple(Integer multiple) {
        this.multiple = multiple;
    }
}
