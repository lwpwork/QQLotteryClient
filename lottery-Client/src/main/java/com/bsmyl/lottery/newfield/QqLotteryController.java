package com.bsmyl.lottery.newfield;

import com.alibaba.fastjson.JSON;
import com.bsmyl.lottery.config.FeignBasicAuthRequestInterceptor;
import com.bsmyl.lottery.core.MapUtile;
import com.bsmyl.lottery.newfield.bet.QqLotteryBet;
import com.bsmyl.lottery.scor.Result;
import com.bsmyl.lottery.scor.ResultGenerator;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * describe:
 *
 * @author LiuWenPing
 * @date 2019/02/28
 */
@Controller
@RequestMapping("/qqLotteryController")
public class QqLotteryController {


    @RequestMapping("/initLotteryConfig")
    @ResponseBody
    public Result initLotteryConfig(@RequestBody QqLotteryConfig qqLotteryConfig) throws Exception {
        QqLotteryBet.lotteryConfig = MapUtile.objectToMap(qqLotteryConfig);
        return ResultGenerator.genSuccessMsgResult("设置成功");
    }

    @RequestMapping("/initLotteryConfig02")
    @ResponseBody
    public Result initLotteryConfig02(@RequestBody QqLotteryConfig qqLotteryConfig) throws Exception {
        QqLotteryBet.lotteryConfig02 = MapUtile.objectToMap(qqLotteryConfig);
        return ResultGenerator.genSuccessMsgResult("设置成功");
    }

    @RequestMapping("/login")
    @ResponseBody
    public Result login(String sessionID){
        FeignBasicAuthRequestInterceptor.sessionID = sessionID;
        return ResultGenerator.genSuccessMsgResult("登录成功");

    }



    public static void main(String[] args) {
        QqLotteryConfig qqLotteryConfig = new QqLotteryConfig();
        System.out.println(JSON.toJSONString(qqLotteryConfig));
    }


}
