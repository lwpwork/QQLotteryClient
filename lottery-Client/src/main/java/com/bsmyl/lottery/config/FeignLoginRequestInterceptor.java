package com.bsmyl.lottery.config;

import feign.RequestInterceptor;
import feign.RequestTemplate;

import java.util.*;

/**
 * describe:
 *
 * @author LiuWenPing
 * @date 2019/03/05
 */
public class FeignLoginRequestInterceptor implements RequestInterceptor {
    @Override
    public void apply(RequestTemplate requestTemplate) {
        Map<String, Collection<String>> map = new HashMap<>();
        List<String> accept = new ArrayList<>();
        String Accept01 = "application/json, text/javascript, */*";
        accept.add(Accept01);
        map.put("Accept", accept);
        List<String> contentType = new ArrayList<>();
        contentType.add("application/x-www-form-urlencoded");
        //contentType.add("charset=UTF-8");
        map.put("Content-Type", contentType);
        List<String> origin = new ArrayList<>();
        origin.add("https://www.59001122.com");
        map.put("origin", origin);
        List<String> referer = new ArrayList<>();
        referer.add("https://www.59001122.com/default.html");
        map.put("referer", referer);
        List<String> userAgent = new ArrayList<>();
        userAgent.add("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36");
        map.put("User-Agent", userAgent);
        requestTemplate.headers(map);
    }
}
