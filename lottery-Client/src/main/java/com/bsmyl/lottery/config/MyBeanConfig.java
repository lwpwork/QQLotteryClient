package com.bsmyl.lottery.config;

import com.bsmyl.lottery.Service.BetPlan;
import com.bsmyl.lottery.Service.ImportLotteryData;
import com.bsmyl.lottery.Service.impl.BetPlanImpl;
import com.bsmyl.lottery.Service.impl.ImportLotteryDataImpl;
import com.bsmyl.lottery.model.QQLottery;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * describe:
 *
 * @author LiuWenPing
 * @date 2019/02/19
 */


@Configuration
public class MyBeanConfig {




    @Bean
    public ImportLotteryData<QQLottery> qqImportLotteryData(){
        return new ImportLotteryDataImpl<QQLottery>();
    }
    @Bean
    public BetPlan betPlan(){
        return new BetPlanImpl();
    }

}
