package com.bsmyl.lottery.config;

import com.alibaba.fastjson.JSON;
import feign.RequestInterceptor;
import feign.RequestTemplate;

import java.util.*;

/**
 * describe:
 *
 * @author LiuWenPing
 * @date 2019/02/28
 */
public class FeignBasicAuthRequestInterceptor implements RequestInterceptor {

    public static String sessionID = "";



    @Override
    public void apply(RequestTemplate requestTemplate) {
        Map<String, Collection<String>> map = new HashMap<>();
        List<String> accept = new ArrayList<>();
        String Accept01 = "application/json, text/javascript, */*";
        accept.add(Accept01);
        String Accept02 = "q=0.01";
        accept.add(Accept02);
        map.put("Accept", accept);
        List<String> contentType = new ArrayList<>();
        contentType.add("application/x-www-form-urlencoded");
        contentType.add("charset=UTF-8");
        map.put("Content-Type", contentType);
        if (sessionID != null && !sessionID.equals("")) {
            List<String> cookie = new ArrayList<>();
            cookie.add("SESSION="+sessionID);
            map.put("Cookie", cookie);
        }
        List<String> origin = new ArrayList<>();
        origin.add("http://xtd.e5903y.com");
        map.put("Origin", origin);
        List<String> referer = new ArrayList<>();
        referer.add("http://xtd.e5903y.com/game/lottery/play.html?qqmin");
        map.put("Referer", referer);
        List<String> userAgent = new ArrayList<>();
        userAgent.add("Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36");
        map.put("User-Agent", userAgent);
        requestTemplate.headers(map);
    }


    public static void main(String[] args) {
        Map<String, Collection<String>> map = new HashMap<>();
        List<String> accept = new ArrayList<>();
        String Accept01 = "application/json, text/javascript, */*";
        accept.add(Accept01);
        String Accept02 = "q=0.01";
        accept.add(Accept02);
        map.put("Accept", accept);
        List<String> contentType = new ArrayList<>();
        contentType.add("application/x-www-form-urlencoded");
        contentType.add("charset=UTF-8");
        map.put("Content-Type", contentType);
        List<String> cookie = new ArrayList<>();
        cookie.add("SESSION=e10dbecb-ce4b-4f4a-9bf9-439f2bced460");
        map.put("Cookie", cookie);
        List<String> origin = new ArrayList<>();
        origin.add("http://xtd.e5903y.com");
        map.put("Origin", origin);
        List<String> referer = new ArrayList<>();
        referer.add("http://xtd.e5903y.com/game/lottery/play.html?qqmin");
        map.put("Referer", referer);
        List<String> userAgent = new ArrayList<>();
        userAgent.add("Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36");
        map.put("User-Agent", userAgent);
        System.out.println(JSON.toJSONString(map));
    }


}
