package com.bsmyl.lottery.config;

import com.bsmyl.lottery.model.PkPlanModel;

/**
 * describe:
 *
 * @author LiuWenPing
 * @date 2019/02/15
 */
public class LotteryConfig {

    public static String Token;

    public static boolean allowPk2fBet = true;

    public static boolean pk10StartBet = false;

    public static PkPlanModel[] pkPlanModels = new PkPlanModel[11];

    public static int[] pkLoseCount = new int[]{0,0,0,0,0,0,0,0,0,0,0};

    public static boolean stopLoss = false;  //止损，true：止损，停止押注。   false：不止损，继续押注。


    //押注倍率。
    public static int[] powers = new int[]{1,2,4,8,16,32,64,128,256,512,1024};

    //基础金额。 下注金额 = baseAmount * power
    public static int baseAmount = 10; //两块





}
