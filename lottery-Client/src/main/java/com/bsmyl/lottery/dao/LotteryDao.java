package com.bsmyl.lottery.dao;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import com.bsmyl.lottery.model.Lottery;
import org.mybatis.spring.annotation.MapperScan;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper
public interface LotteryDao {
    int insert(@Param("pojo") Lottery pojo);

    int insertSelective(@Param("pojo") Lottery pojo);

    int insertList(@Param("pojos") List<Lottery> pojo);

    int update(@Param("pojo") Lottery pojo);

    List<Lottery> lotteryList(@Param("lotteryId") Long lotteryId);

    Lottery getLottery(@Param("lotteryId")Long lottery_id, @Param("issue")Long issue);

    int lotteryCount(@Param("lotteryId")Long lottery_id);

    void delete();

    List<Lottery> lotteryListPage(@Param("lotteryId") Long lotteryId);

    Lottery getQQLottery();




}
