package com.bsmyl.lottery.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.bsmyl.lottery.model.QQLottery;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.stereotype.Component;

@Component
@Mapper
public interface QQLotteryDao {
    int insert(@Param("pojo") QQLottery pojo);

    int insertSelective(@Param("pojo") QQLottery pojo);

    int insertList(@Param("pojos") List<QQLottery> pojo);

    int update(@Param("pojo") QQLottery pojo);

    List<Map<String,Object>> theChineseBoxerList(@Param("type01")int type01, @Param("type02") int type02, @Param("groupTypeStr") String groupTypeStr);

    List<Map<String,Object>> inTheFormerList(@Param("groupTypeStr")String groupType);

    List<Map<String,Object>> daXiaoDanShuang(@Param("groupTypeStr")String groupTypeStr);

    Map<String,Object> daxiaoSerialNumber(@Param("ariseCount")int ariseCount, @Param("ranking")String ranking);

    Map<String,Object> danShuangSerialNumber(@Param("ariseCount")int ariseCount, @Param("ranking")String ranking);

    QQLottery getQQLottery();

    List<QQLottery> qqLotteryList();

    List<QQLottery> qqLotterys(@Param("startTime")Date startTime, @Param("endTime")Date endTime);

    List<QQLottery> carLotterys(@Param("startTime") Date startTime, @Param("endTime") Date endTime);
}
