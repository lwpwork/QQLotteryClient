package com.bsmyl.lottery.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import com.bsmyl.lottery.model.PkPlanModel;
import org.springframework.stereotype.Component;

@Mapper
@Component
public interface PkPlanModelDao {
    int insert(@Param("pojo") PkPlanModel pojo);

    int insertSelective(@Param("pojo") PkPlanModel pojo);

    int insertList(@Param("pojos") List<PkPlanModel> pojo);

    int update(@Param("pojo") PkPlanModel pojo);

    List<PkPlanModel> pkPlanModelList(@Param("lotteryId")Long lotteryId, @Param("ranking")Integer ranking);

    Double getWinRate(@Param("wrongCount") Integer wrongCount, @Param("gameNums") Integer gameNums);
}
