package com.bsmyl.lottery.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

import com.bsmyl.lottery.model.PkDetail;
import org.springframework.stereotype.Component;

@Component
@Mapper
public interface PkDetailDao {
    int insert(@Param("pojo") PkDetail pojo);

    int insertSelective(@Param("pojo") PkDetail pojo);

    int insertList(@Param("pojos") List<PkDetail> pojo);

    int update(@Param("pojo") PkDetail pojo);

    List<Map<String,Object>> numFrequencyList(@Param("lottery_id")Long lottery_id, @Param("num_size")Integer num_size,
                                              @Param("nums")Integer[] nums, @Param("type")Integer type);
}
