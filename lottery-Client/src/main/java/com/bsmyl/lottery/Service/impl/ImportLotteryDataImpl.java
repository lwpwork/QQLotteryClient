package com.bsmyl.lottery.Service.impl;

import com.bsmyl.lottery.Service.AnalysisDataAble;
import com.bsmyl.lottery.Service.ImportLotteryData;
import com.bsmyl.lottery.play.PlayController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * describe:
 *
 * @author LiuWenPing
 * @date 2019/02/18
 */
@Service
public class ImportLotteryDataImpl<T> implements ImportLotteryData<T> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ImportLotteryDataImpl.class);


    /**
     * testURI:C:\Users\Administrator\Desktop\1.txt
     *
     * @param fileName     导入数据文件地址
     * @param analysisDataAble
     * @return
     */
    @Override
    public List<T> importData(String fileName, AnalysisDataAble analysisDataAble) {
        File file = new File(fileName);
        List<T> anyObjList = new ArrayList<>();
        BufferedReader reader = null;
        try {
            //LOGGER.info("以行为单位读取文件内容，一次读一整行：");
            reader = new BufferedReader(new FileReader(file));
            String tempString = null;
            int line = 1;
            // 一次读入一行，直到读入null为文件结束
            while ((tempString = reader.readLine()) != null) {
                // 显示行号
                LOGGER.info("line " + line + ": " + tempString);
                T anyObj = (T) analysisDataAble.dataAnalysis(tempString);
                anyObjList.add(anyObj);
                line++;
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e1) {
                }
            }
        }

        return anyObjList;
    }

}
