package com.bsmyl.lottery.Service.impl;

import com.bsmyl.lottery.Service.QqLotteryService;
import com.bsmyl.lottery.dao.QQLotteryDao;
import com.bsmyl.lottery.model.QQLottery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * describe:
 *
 * @author LiuWenPing
 * @date 2019/03/07
 */
@Service
public class QqLotteryServiceImpl implements QqLotteryService {

    @Autowired
    private QQLotteryDao qqLotteryDao;


    @Override
    public void simulationBet() {
        Date startTime = getDate("2019-03-17 00:00:00");
        Date endTime = getDate("2019-03-24 00:00:00");
        //List<QQLottery> qqLotteries = qqLotteryDao.qqLotteryList();
        List<QQLottery> qqLotteries = qqLotteryDao.carLotterys(startTime, endTime);
        final int cardinalNum = 11;
        for (int k = 1; k <= 10; k++) {
            int index = 0;//期数
            int[] ariseCount = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            int[] bet = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            boolean[] isbet = new boolean[]{false, false, false, false, false, false, false, false, false, false};
            int[] winCount = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            int num = -1;
            int[] firstIndex = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            for (QQLottery qqLottery :
                    qqLotteries) {
                num = getNum(k, qqLottery);
                for (int i = 0; i < 10; i++) {
                    if (num == i + 1) {
                        if (ariseCount[i] == 0) {
                            //firstIndex[i] = index;
                        }
                    }
                    if (isbet[i]) {
                        if (num == i + 1) {//赢了
                            winCount[i]++;
                            isbet[i] = false;
                        } else { //输了
                            isbet[i] = false;
                        }
                    }
                    if (num == i + 1) {//上一局出现了吗 ??
                        ariseCount[i]++;
                    }
                    if (firstIndex[i] + ariseCount[i] * cardinalNum <= index && index > 500) {
                        bet[i]++;//押注结束后默认为还未赢
                        isbet[i] = true;
                    }
                }
                index++;
            }
            System.out.println("第"+k+"名:");
            for (int i = 0; i < 10; i++) {
                System.out.println("号码:" + (i + 1) + ",winCount:" + winCount[i] + ",betCount:" + bet[i]);
            }
        }

    }

    public Integer getNum(Integer index, QQLottery qqLottery) {
        switch (index) {
            case 1:
                return qqLottery.getOne();
            case 2:
                return qqLottery.getTwo();
            case 3:
                return qqLottery.getThree();
            case 4:
                return qqLottery.getFour();
            case 5:
                return qqLottery.getFive();
            case 6:
                return qqLottery.getSix();
            case 7:
                return qqLottery.getSeven();
            case 8:
                return qqLottery.getEight();
            case 9:
                return qqLottery.getNine();
            default:
                return qqLottery.getTen();
        }

    }

    /**
     * 龙虎,模拟押注,看最对连错数
     */
    @Override
    public void simulationLhBet() {
        Date startTime = getDate("2019-02-17 00:00:00");
        Date endTime = getDate("2019-02-18 00:00:00");
        List<QQLottery> qqLotteries = qqLotteryDao.qqLotterys(startTime, endTime);
        //List<QQLottery> qqLotteries = qqLotteryDao.qqLotteryList();
        Integer betcount = 0;
        Integer win = 0;
        for (int h = 2; h <= 2; h++) {
            int[] maxdCount = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            int[] lxMistakeCount = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};  //连错数
            int[] arise = new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
            int[] bet = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            int[] winCount = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            boolean[] isBet = new boolean[]{false, false, false, false, false, false, false, false, false, false};
            int[] count02 = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            int[] count03 = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            int[] count04 = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            int[] count05 = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            int[] count06 = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            int[] count07 = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            int[] count08 = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            int[] count09 = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            int[] count10 = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            int[] count11 = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            int[] count12 = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            int[] count13 = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            int[] countgt14 = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            int[] lc2BetCount = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            int[] lc3BetCount = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            int[] lc4BetCount = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            int[] lc5BetCount = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            int[] lc6BetCount = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            int[] lc7BetCount = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            int[] lc8BetCount = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            int[] lc9BetCount = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            QQLottery prevLottery = null;
            int prevType = -1;
            for (QQLottery qqLottery :
                    qqLotteries) {
                for (int i = 0; i < 10; i++) {
                    int type = getType(qqLottery, i + 1);
                    if (prevLottery != null) {
                        prevType = getType(prevLottery, i + 1);
                        if (arise[i] == h) {
                            bet[i]++;//押注
                            isBet[i] = true;
                        }
                        //验证是否押注.押注了验证是否赢了
                        if (isBet[i]) {
                            //判断是输赢
                            if (type == prevType || type == 3) {//输了
                                lxMistakeCount[i]++;
                            } else {//赢了
                                winCount[i]++;
                                if (lxMistakeCount[i] > maxdCount[i]) {
                                    maxdCount[i] = lxMistakeCount[i];
                                }
                                lxMistakeCount[i] = 0;

                            }
                            if (lxMistakeCount[i] == 2) {
                                count02[i]++;
                            }
                            if (lxMistakeCount[i] == 3) {
                                count03[i]++;
                            }
                            if (lxMistakeCount[i] == 4) {
                                count04[i]++;
                            }
                            if (lxMistakeCount[i] == 5) {
                                count05[i]++;
                            }
                            if (lxMistakeCount[i] == 6) {
                                count06[i]++;
                            }
                            if (lxMistakeCount[i] == 7) {
                                count07[i]++;
                            }
                            if (lxMistakeCount[i] == 8) {
                                count08[i]++;
                            }
                            if (lxMistakeCount[i] == 9) {
                                count09[i]++;
                            }
                            if (lxMistakeCount[i] == 10) {
                                count10[i]++;
                            }
                            if (lxMistakeCount[i] == 11) {
                                count11[i]++;
                            }
                            if (lxMistakeCount[i] == 12) {
                                count12[i]++;
                            }
                            if (lxMistakeCount[i] == 13) {
                                count13[i]++;
                            }
                            if (lxMistakeCount[i] >= 2) {
                                lc2BetCount[i]++;
                            }
                            if (lxMistakeCount[i] >= 3) {
                                lc3BetCount[i]++;
                            }
                            if (lxMistakeCount[i] >= 4) {
                                lc4BetCount[i]++;
                            }
                            if (lxMistakeCount[i] >= 5) {
                                lc5BetCount[i]++;
                            }
                            if (lxMistakeCount[i] >= 6) {
                                lc6BetCount[i]++;
                            }
                            if (lxMistakeCount[i] >= 7) {
                                lc7BetCount[i]++;
                            }
                            if (lxMistakeCount[i] >= 8) {
                                lc8BetCount[i]++;
                            }
                            if (lxMistakeCount[i] >= 9) {
                                lc9BetCount[i]++;
                            }
                            if (lxMistakeCount[i] >= 14) {
                                countgt14[i]++;
                            }
                            isBet[i] = false;
                        }
                        if ((type == prevType && type != 3)) {//连续出现
                            arise[i]++;
                        } else {
                            arise[i] = 1;
                        }
                    }
                }
                prevLottery = qqLottery;
            }
            System.out.println(h + "连虎,开始计算");
            for (int i = 0; i < 10; i++) {
                win += winCount[i];
                betcount += bet[i];
                /*System.out.println("号码:"+i+"\n最大连错数:" + maxdCount[i] + "\n胜利次数:" + winCount[i] + "\n下注数:" + bet[i] +
                        "\n2连错次数:" + count02[i] + "\n3连错次数:" + count03[i] + "\n4连错次数:" + count04[i] + "\n5连错次数:" + count05[i] +
                        "\n6连错次数:" + count06[i] + "\n7连错次数:" + count07[i] + "\n8连错次数:" + count08[i] + "\n9连错次数:" + count09[i] +
                        "\n10连错次数:" + count10[i] + "\n11连错次数:" + count11[i] + "\n12连错次数:" + count12[i] + "\n13连错次数:" + count13[i] +"\n14以上被吃"+ countgt14[i]+
                        "\n1连错后开始下注次数:" + lc2BetCount[i]+"\n2连错后开始下注次数:" + lc3BetCount[i]+"\n3连错后开始下注次数:" + lc4BetCount[i]+"\n4连错后开始下注次数:" + lc5BetCount[i]+
                        "\n5连错后开始下注次数:" + lc6BetCount[i]+"\n6连错后开始下注次数:" + lc7BetCount[i]+"\n7连错后开始下注次数:" + lc8BetCount[i]+"\n8连错后开始下注次数:" + lc9BetCount[i]
                );
                System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");*/
            }
            //System.out.println("======================================================================================================================================================");
        }
        System.out.println("下注数:" + betcount + ",胜利次数:" + win);


    }

    @Override
    public void simulationdLhBet() {
        Date startTime = getDate("2019-03-30 00:00:00");
        Date endTime = getDate("2019-03-31 00:00:00");
        List<QQLottery> qqLotteries = qqLotteryDao.qqLotterys(startTime,endTime);
        //List<QQLottery> qqLotteries = qqLotteryDao.qqLotteryList();

        for (int k = 0; k < 8; k++) {
            Integer[] nums = getNums(k + 1);
            int [] loseCount = new int[]{0, 0, 0, 0, 0};
            int[] arise = new int[]{1, 1, 1, 1, 1};
            int[] bet = new int[]{0, 0, 0, 0, 0};
            int[] winCount = new int[]{0, 0, 0, 0, 0};
            boolean[] isBet = new boolean[]{false, false, false, false, false};
            QQLottery prevQQLottery = null;

            for (QQLottery qqLottery :
                    qqLotteries) {
                for (int i = 0; i < 3; i++) {
                    int type = isWin(qqLottery,nums,i+1);//getType(qqLottery, i + 1);
                    if (prevQQLottery != null) {
                        //int prevType = getNum(i + 1,prevQQLottery);
                        if (arise[i] >= 1) {
                            bet[i]++;//押注
                            isBet[i] = true;
                        }
                        if (isBet[i]) {
                            if (type!=0) {//赢了
                                winCount[i]++;
                            } else {//输了
                                //不记录赢得次数
                            }
                            isBet[i] = false;
                        }
                        if (type == 0) {//输了
                            if (arise[i] <= 15) {
                                arise[i]++;
                            } else {
                                loseCount[i]++;
                                arise[i] = 1;
                            }
                        } else {
                            //arise[i] = 1;
                            if (arise[i] > 15) {
                                loseCount[i]++;
                            }
                            arise[i] = 1;
                        }
                    }else {
                    /*if (type == 3) {
                        arise[i] = 0;
                    }*/
                    }
                }
                prevQQLottery = qqLottery;
            }
            int win = 0;
            int betcount = 0;
            int lose = 0;
            for (int i = 0; i < 3; i++) {
                System.out.print("第"+(i+1)+"名,"+"下注数:" + bet[i] + ",胜利次数:" + winCount[i]+",四次不中次数:"+(loseCount[i]+1)+"\t");
                win += winCount[i];
                betcount += bet[i];
                lose += (loseCount[i]+1);
            }
            System.out.println();
            //System.out.println("下注数:" + betcount + ",胜利次数:" + win+",四次不中次数:"+lose);
        }

    }


    public Integer[] getNums(Integer index){
        switch (index) {
            case 1:
                return new Integer[]{0, 1, 2, 3, 4};
            case 2:
                return new Integer[]{1, 2, 3, 4, 5};
            case 3:
                return new Integer[]{2, 3, 4, 5, 6};
            case 4:
                return new Integer[]{3, 4, 5, 6, 7};
            case 5:
                return new Integer[]{4, 5, 6, 7, 8};
            case 6:
                return new Integer[]{5, 6, 7, 8, 9};
            case 7:
                return new Integer[]{1, 3, 5, 7, 9};
            default:
                return new Integer[]{0, 2, 4, 6, 8};
        }
    }


    public Integer isWin(QQLottery qqLottery,Integer [] nums,Integer index){
        int result = 0;
        boolean numOneZhong = false;
        boolean numTwoZhong = false;
        switch (index) {
            case 1://万千
                for(Integer num :
                        nums) {
                    if (qqLottery.getOne().equals(num)) {
                        numOneZhong =true;
                    }
                    if (qqLottery.getTwo().equals(num)) {
                        numTwoZhong = true;
                    }
                }
                break;
            case 2: // 千十
                for(Integer num :
                        nums) {
                    if (qqLottery.getTwo().equals(num)) {
                        numOneZhong =true;
                    }
                    if (qqLottery.getFour().equals(num)) {
                        numTwoZhong = true;
                    }
                }
                break;
            case 3: // 十个
                for(Integer num :
                        nums) {
                    if (qqLottery.getFour().equals(num)) {
                        numOneZhong =true;
                    }
                    if (qqLottery.getFive().equals(num)) {
                        numTwoZhong = true;
                    }
                }
                break;
            default:
                result = 0;
                break;
        }
        if (numOneZhong && numTwoZhong) {
            result = 1;
        }
        return result;

    }



    public static void main(String[] args) throws ParseException {
        String dateStr = "2019-03-14 00:00:00";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date date = simpleDateFormat.parse(dateStr);
        System.out.println(date);
    }

    public Date getDate(String dateStr) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        try {
            Date date = simpleDateFormat.parse(dateStr);
            return date;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;

    }

    public Integer getWqCha(QQLottery qqLottery){
        return Math.abs(qqLottery.getOne()-qqLottery.getTwo());
    }





    /**
     * @param qqLottery
     * @param index
     * @return 1-龙;2-虎;3-和
     */
    private Integer getType(QQLottery qqLottery, int index) {
        switch (index) {
            case 1:
                if (qqLottery.getOne() > qqLottery.getTwo()) {
                    return 1;
                } else if (qqLottery.getOne() < qqLottery.getTwo()) {
                    return 2;
                } else {
                    return 3;
                }
            case 2:
                if (qqLottery.getOne() > qqLottery.getThree()) {
                    return 1;
                } else if (qqLottery.getOne() < qqLottery.getThree()) {
                    return 2;
                } else {
                    return 3;
                }
            case 3:
                if (qqLottery.getOne() > qqLottery.getFour()) {
                    return 1;
                } else if (qqLottery.getOne() < qqLottery.getFour()) {
                    return 2;
                } else {
                    return 3;
                }
            case 4:
                if (qqLottery.getOne() > qqLottery.getFive()) {
                    return 1;
                } else if (qqLottery.getOne() < qqLottery.getFive()) {
                    return 2;
                } else {
                    return 3;
                }
            case 5:
                if (qqLottery.getTwo() > qqLottery.getThree()) {
                    return 1;
                } else if (qqLottery.getTwo() < qqLottery.getThree()) {
                    return 2;
                } else {
                    return 3;
                }
            case 6:
                if (qqLottery.getTwo() > qqLottery.getFour()) {
                    return 1;
                } else if (qqLottery.getTwo() < qqLottery.getFour()) {
                    return 2;
                } else {
                    return 3;
                }
            case 7:
                if (qqLottery.getTwo() > qqLottery.getFive()) {
                    return 1;
                } else if (qqLottery.getTwo() < qqLottery.getFive()) {
                    return 2;
                } else {
                    return 3;
                }
            case 8:
                if (qqLottery.getThree() > qqLottery.getFour()) {
                    return 1;
                } else if (qqLottery.getThree() < qqLottery.getFour()) {
                    return 2;
                } else {
                    return 3;
                }
            case 9:
                if (qqLottery.getThree() > qqLottery.getFive()) {
                    return 1;
                } else if (qqLottery.getThree() < qqLottery.getFive()) {
                    return 2;
                } else {
                    return 3;
                }
            default:
                if (qqLottery.getFour() > qqLottery.getFive()) {
                    return 1;
                } else if (qqLottery.getFour() < qqLottery.getFive()) {
                    return 2;
                } else {
                    return 3;
                }
        }
    }

    public Integer getWinCount(int[] ariseCount) {
        Integer winCount = 0;
        for (int i = 0; i < ariseCount.length; i++) {
            winCount += ariseCount[i];
        }
        return winCount - 1;
    }
}


