package com.bsmyl.lottery.Service.impl;

import com.bsmyl.lottery.Service.StopLossService;
import com.bsmyl.lottery.dao.LotteryDao;
import com.bsmyl.lottery.dao.PkPlanModelDao;
import com.bsmyl.lottery.model.Lottery;
import com.bsmyl.lottery.model.PkPlanModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * describe:
 *
 * @author LiuWenPing
 * @date 2019/03/01
 */
@Service
public class StopLossServiceImpl implements StopLossService {

    @Autowired
    private LotteryDao lotteryDao;


    @Autowired
    private PkPlanModelDao pkPlanModelDao;





    @Override
    public boolean isStopLoss(Integer ranking) {
        List<PkPlanModel> planModels = pkPlanModelDao.pkPlanModelList(1404L,ranking);
        int winCount = 0;
        if (planModels == null || planModels.size() < 15) {//小于15期怎么办 ??

            for (PkPlanModel planModel :
                    planModels) {
                if (planModel.isWinPlan()) {
                    winCount++;
                }
            }
            double winRate = (double) winCount / (double) planModels.size() * 100;
            if (winRate <= 55) {//超过止损预警
                return true;
            }
        } else {
            int index = 0;
            int contCount = 0;  //连挂次数
            int ariseCount = 0;
            //正在连续错着的计划次数.
            int ariseIng = 0;
            boolean firstArise = false;
            for (PkPlanModel planModel :
                    planModels) {
                if (planModel.isWinPlan()) {
                    if (ariseCount >= 3) { //连错次数大于3.记录次数.
                        contCount++;
                    }
                    ariseCount = 0; //本局赢了,连续出现错误的次数中断.
                    winCount++;
                } else {//本期计划输了.
                    if (index == ariseIng) {
                        ariseIng++;
                        if (ariseIng == 3) { //当开始有3连错的时候记录为true.后面用会用到这个.当 firstArise = true的时候,则不校验winCount
                            firstArise = true;
                        }
                    }
                    ariseCount++;
                }
                index++;
            }

            if (!firstArise && winCount < 7) {//超过止损预警,.当 firstArise = true的时候,则不校验winCount
                return true;
            }
            if (ariseCount == 1 && contCount >= 2 ) {
                return true;
            }
            if (ariseIng >= 3 && contCount >= 2) {
                return true;
            }
        }
        return false;
    }
}
