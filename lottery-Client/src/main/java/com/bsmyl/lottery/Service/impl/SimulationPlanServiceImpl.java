package com.bsmyl.lottery.Service.impl;

import com.alibaba.fastjson.JSON;
import com.bsmyl.lottery.Service.SimulationPlanService;
import com.bsmyl.lottery.config.LotteryConfig;
import com.bsmyl.lottery.core.SimulationPlanConfig;
import com.bsmyl.lottery.dao.LotteryDao;
import com.bsmyl.lottery.dao.PkPlanModelDao;
import com.bsmyl.lottery.model.Lottery;
import com.bsmyl.lottery.model.PkPlanModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * describe:提供模拟自己的计划的一些方案.
 * 本次模拟方案为自动监测,定量间隔
 *
 * @author LiuWenPing
 * @date 2019/04/29
 */
@Service
public class SimulationPlanServiceImpl implements SimulationPlanService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimulationPlanServiceImpl.class);

    @Autowired
    private LotteryDao lotteryDao;

    @Autowired
    private PkPlanModelDao pkPlanModelDao;

    Map<Long, PkPlanModel> SimulationBet = new HashMap<>();

    @Override
    public void qqLotterySimulationPlan() {
        Lottery lottery = getLottery();//lotteryDao.getQQLottery();

        Lottery localLottery = lotteryDao.getLottery(1025L, lottery.getIssue());

        PkPlanModel[] pkPlanModels = LotteryConfig.pkPlanModels;
        List<PkPlanModel> planModels = new ArrayList<>();
        for (int i = 1; i < pkPlanModels.length; i++) {
            if (pkPlanModels[i] != null && SimulationPlanConfig.isStart && SimulationPlanConfig.isAllowBet && !SimulationPlanConfig.isBet) { //计划不为空，并且本期还没有下注
                //验证上一期的 计划是否正确
                Long issue = pkPlanModels[i].getIssue();
                if (lottery.getIssue().equals(issue - 1)) {//本期开奖期数= 计划期数-1 说明计划是新出的计划，可以押注
                    //TODO 下注扣款
                    //planModels.add(pkPlanModels[i]);
                    SimulationPlanConfig.bet();

                }
            }
        }

    }

    public static Lottery getLottery(){
        String url = "http://www.off0.com/list";
        StringBuilder sb = new StringBuilder();
        Lottery lottery = new Lottery();
        try {
            URL urlObject = new URL(url);
            URLConnection uc = urlObject.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
            String inputLine = null;
            while ( (inputLine = in.readLine()) != null) {
                sb.append(inputLine);
            }
            in.close();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<Map> objectMap = JSON.parseArray(sb.toString(), Map.class);
        lottery.setIssue(Long.valueOf(objectMap.get(0).get("issue").toString()));
        lottery.setLotteryId(1025L);
        lottery.setOpenNum(objectMap.get(0).get("result").toString());
        lottery.setOpenTime(objectMap.get(0).get("time").toString());
        //return JSON.parseArray(sb.toString(), Map.class);
        return lottery;
    }


}
