package com.bsmyl.lottery.Service.impl;

import com.alibaba.fastjson.JSON;
import com.bsmyl.lottery.Service.AnalysisDataAble;
import com.bsmyl.lottery.Service.BetAble;
import com.bsmyl.lottery.config.LotteryConfig;
import com.bsmyl.lottery.feign.LotteryFeignClient;
import com.bsmyl.lottery.model.*;
import com.bsmyl.lottery.play.PlayController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * describe:
 *
 * @author LiuWenPing
 * @date 2019/02/25
 */
@Service
public class betAbleImpl implements BetAble {

    @Autowired
    private LotteryFeignClient lotteryFeignClient;

    private static final Logger LOGGER = LoggerFactory.getLogger(betAbleImpl.class);
    @Override
    public void lotteryBet(List<PkPlanModel> pkPlanModels, Result result, AnalysisDataAble<PkPlanModel> analysisDataAble) {
        List<Bet> betList = new ArrayList<>();
        Long playId = 141131010L;
        //解析押注信息
        betList = analysisDataAble.betAnalysis(pkPlanModels);
        Lottery lottery = result.getData().getHistoryList().get(0);
        Long issue = lottery.getIssue() + 1;
        int randomNum = (int) ((Math.random() * 9 + 1) * 1000);
        Order order = new Order("LotteryUserBet", lottery.getLotteryId().intValue(), issue.toString(), JSON.toJSONString(betList), String.valueOf(System.currentTimeMillis() + randomNum) + randomNum, IssueServiceImpl.token);
        LOGGER.info(JSON.toJSONString(order));
        Result res = lotteryFeignClient.LotteryUserBet(order);
        LOGGER.info(JSON.toJSONString(res));
    }
}
