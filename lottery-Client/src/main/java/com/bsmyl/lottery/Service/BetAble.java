package com.bsmyl.lottery.Service;

import com.bsmyl.lottery.model.PkPlanModel;
import com.bsmyl.lottery.model.Result;

import java.util.List;

public interface BetAble {

    void lotteryBet(List<PkPlanModel> pkPlanModels, Result result, AnalysisDataAble<PkPlanModel> analysisDataAble);


}
