package com.bsmyl.lottery.Service.impl;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.bsmyl.lottery.Service.IssueService;
import com.bsmyl.lottery.Service.PlanService;
import com.bsmyl.lottery.Service.StopLossService;
import com.bsmyl.lottery.config.LotteryConfig;
import com.bsmyl.lottery.dao.LotteryDao;
import com.bsmyl.lottery.dao.PkDetailDao;
import com.bsmyl.lottery.dao.PkPlanModelDao;
import com.bsmyl.lottery.feign.LotteryFeignClient;
import com.bsmyl.lottery.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

/**
 * describe:
 * 押注采用倍投押注：1、2、4、10、30、60、120
 *
 * @author LiuWenPing
 * @date 2019/02/21
 */
@Service
@Transactional
public class IssueServiceImpl implements IssueService {


    private static final Logger LOGGER = LoggerFactory.getLogger(IssueServiceImpl.class);

    @Autowired
    private LotteryDao lotteryDao;


    @Autowired
    private PkDetailDao pkDetailDao;

    @Autowired
    private LotteryFeignClient lotteryFeignClient;

    @Autowired
    private PlanService planService;

    @Autowired
    private PkPlanModelDao pkPlanModelDao;

    @Autowired
    private StopLossService stopLossService;



    public static String token;


    @Override
    public void initLotteryInfo() {
        LotteryInfoParam lotteryInfoParam = new LotteryInfoParam(token, 1404, 1);
        Result result = lotteryFeignClient.LotteryHistoryOpenInfo(JSON.toJSONString(lotteryInfoParam));
        //System.out.println(JSON.toJSONString(result));
        if (result.getData().getHistoryList() == null) {
            return;
        }
        Lottery lottery = result.getData().getHistoryList().get(0);
        Lottery localLottery = lotteryDao.getLottery(1404L, lottery.getIssue());
        if (localLottery != null) {//说明数据库中已经拥有本期记录
            return;
        }
        lotteryDao.insert(lottery);
        LOGGER.info(JSON.toJSONString(lottery));
        String[] openNums = lottery.getOpenNum().split(",");
        //已经开奖。验证 计划是否正确。  目前只有冠军
        PkPlanModel[] pkPlanModels = LotteryConfig.pkPlanModels;
        List<PkPlanModel> plans = new ArrayList<>();

        for (int i = 0; i < openNums.length; i++) {
            PkPlanModel prevPkPlanModel = pkPlanModels[i + 1];
            if (prevPkPlanModel != null && prevPkPlanModel.getIssue().equals(lottery.getIssue())) {
                boolean isWin = false;
                for (int j = 0; j < prevPkPlanModel.getBetNums().length; j++) {
                    if (Integer.valueOf(openNums[i]).equals(prevPkPlanModel.getBetNums()[j])) {
                        isWin = true;
                        break;
                    }
                }
                //倍投倍率增加、止损策略
                if (!isWin) {
                    LotteryConfig.pkLoseCount[prevPkPlanModel.getRanking()]++;
                    //超过5局停止下注
                } else {
                    LotteryConfig.pkLoseCount[prevPkPlanModel.getRanking()] = 0;
                    LotteryConfig.stopLoss = false;  //赢了转换为不止损.
                }
                prevPkPlanModel.setWinPlan(isWin);
                plans.add(prevPkPlanModel);
            }
        }
        if (plans != null && plans.size() > 0) {
            pkPlanModelDao.insertList(plans);
        }
        Long lotteryId = lottery.getLotteryId();
        List<PkDetail> pkDetailList = new ArrayList<>();
        //查询出20条数据
        List<Lottery> lotteryList = lotteryDao.lotteryList(lotteryId);
        for (int i = 1; i <= 10; i++) {
            int[] ariseCounts = new int[10];
            //遍历当前名次中的10辆车，遍历列表中每辆车出现的次数
            for (int j = 1; j <= 10; j++) {
                for(Lottery  lottery1 :
                        lotteryList) {
                    String[] types = lottery1.getOpenNum().split(",");
                    int num = Integer.valueOf(types[i-1]);
                    if (num == j) {
                        ariseCounts[j-1]++;
                    }
                }
                PkDetail pkDetail = new PkDetail(lottery.getIssue(), 1404L, i,ariseCounts[j-1], j, DateUtil.parse(lottery.getOpenTime()));
                pkDetailList.add(pkDetail);
            }
        }
        pkDetailDao.insertList(pkDetailList);
        LOGGER.info("数据添加完成！");
        //数据存储完毕后，进行计划生成

        int count = lotteryDao.lotteryCount(1404L);

        if (count > 25) {//开始生成计划
            for (int i = 1; i <= 1; i++) {
                PkPlanModel pkPlanModel = planService.genPkPlan(i);
            }
            //计划生成完毕，游戏打开，允许下注。
            LotteryConfig.allowPk2fBet = true;
        }
        //PkPlanModel pkPlanModel = planService.genPkPlan(1);

    }



}
