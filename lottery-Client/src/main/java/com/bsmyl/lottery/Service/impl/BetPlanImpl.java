package com.bsmyl.lottery.Service.impl;

import com.bsmyl.lottery.Service.BetPlan;
import com.bsmyl.lottery.model.BetPlanModel;
import com.bsmyl.lottery.model.EnuPosition;
import com.bsmyl.lottery.model.Lottery;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * describe:
 *
 * @author LiuWenPing
 * @date 2019/02/14
 */

@Service
public class BetPlanImpl implements BetPlan {

    @Override
    public List<BetPlanModel> genBetPlans(List<Lottery> lotteries) {
        if (lotteries != null && lotteries.size() > 1) {
            Lottery lottery = lotteries.get(0);
            Lottery lottery01 = lotteries.get(1);
            List<BetPlanModel> betPlanModels = new ArrayList<>();
            for (int i = 0; i < 5; i++) {
                char [] position = lottery.getDataExt()[i].toCharArray();
                char [] position01 = lottery01.getDataExt()[i].toCharArray();
                BetPlanModel betPlanModel = new BetPlanModel();

                if (position[0] == position01[0]) {
                    betPlanModel.setbOrS(EnuPosition.position(position[0]));
                }
                if (position[1] == position01[1]) {
                    betPlanModel.setsOrD(EnuPosition.position(position[1]));
                }
                betPlanModels.add(betPlanModel);
            }

            return betPlanModels;
        }
        return null;
    }

    @Override
    public List<BetPlanModel> getContinuousPlan(List<Lottery> lotteries) {


        Lottery prevLottery = null;
        List<BetPlanModel> betPlanModels = new ArrayList<>();
        int [] continuousCount0 = new int[]{1,1,1,1,1};
        boolean [] isContinuous0 = new boolean[]{true,true,true,true,true} ;
        int [] continuousCount1 = new int[]{1,1,1,1,1};
        boolean [] isContinuous1 = new boolean[]{true,true,true,true,true} ;
        int index = 0;
        for(Lottery lottery :
                lotteries) {

            if (prevLottery != null) {//开始预演
                for (int i = 0; i < 5; i++) {
                    char [] position = lottery.getDataExt()[i].toCharArray();
                    char [] position01 = prevLottery.getDataExt()[i].toCharArray();
                    if (position[0] == position01[0] && isContinuous0[i]) {
                        continuousCount0[i]++;
                    } else {
                        isContinuous0[i] = false;
                        //break;
                    }
                    if (position[1] == position01[1] && isContinuous1[i]) {
                        continuousCount1[i]++;
                    } else {
                        isContinuous1[i] = false;
                        //break;
                    }
                }
            }
            prevLottery = lottery;
        }

        for (int i = 0; i < 5; i++) {
            BetPlanModel betPlanModel = new BetPlanModel();
            Lottery lottery = lotteries.get(0);
            char [] position = lottery.getDataExt()[i].toCharArray();
            if (continuousCount0[i] == 3) {//可以出大小计划了
                betPlanModel.setbOrS(EnuPosition.position(position[0]));
            }
            if (continuousCount1[i] == 3) {//可以出单双计划了
                betPlanModel.setsOrD(EnuPosition.position(position[1]));
            }
            betPlanModels.add(betPlanModel);
        }


        return betPlanModels;
    }
}
