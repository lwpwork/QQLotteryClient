package com.bsmyl.lottery.Service.impl;

import com.alibaba.fastjson.JSON;
import com.bsmyl.lottery.Service.AnalysisDataAble;
import com.bsmyl.lottery.Service.BetAble;
import com.bsmyl.lottery.Service.PlayService;
import com.bsmyl.lottery.config.LotteryConfig;
import com.bsmyl.lottery.core.BetUtil;
import com.bsmyl.lottery.dao.LotteryDao;
import com.bsmyl.lottery.feign.LotteryFeignClient;
import com.bsmyl.lottery.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * describe:
 * 所有彩种的押注操作。
 *
 * @author LiuWenPing
 * @date 2019/02/23
 */
@Service
public class PlayServiceImpl implements PlayService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PlayServiceImpl.class);

    @Autowired
    private LotteryDao lotteryDao;

    @Autowired
    private LotteryFeignClient lotteryFeignClient;

    @Autowired
    private BetAble betAble;


    @Override
    public void pk10TowMinuteBet() {
        LotteryInfoParam lotteryInfoParam = new LotteryInfoParam(IssueServiceImpl.token, 1404, 1);
        Result result = lotteryFeignClient.LotteryHistoryOpenInfo(JSON.toJSONString(lotteryInfoParam));
        if (result.getData().getHistoryList() == null) {
            return;
        }
        Lottery lottery = result.getData().getHistoryList().get(0);
        PkPlanModel[] pkPlanModels = LotteryConfig.pkPlanModels;
        List<PkPlanModel> planModels = new ArrayList<>();
        for (int i = 1; i < pkPlanModels.length; i++) {
            if (pkPlanModels[i] != null && LotteryConfig.allowPk2fBet && LotteryConfig.pk10StartBet && !LotteryConfig.stopLoss) { //计划不为空，并且本期还没有下注
                Long issue = pkPlanModels[i].getIssue();
                if (lottery.getIssue().equals(issue-1)) {//本期开奖期数= 计划期数-1 说明计划是新出的计划，可以押注
                    //TODO 将押注信息添加到押注列表中。
                    planModels.add(pkPlanModels[i]);
                }
            }
        }
        //押注
        if (planModels != null && planModels.size() > 0) {
            betAble.lotteryBet(planModels, result, new AnalysisDataAble<PkPlanModel>() {
                @Override
                public List<Bet> betAnalysis(List<PkPlanModel> planModel) {
                    List<Bet> betList = new ArrayList<>();
                    Long playId = 141131010L;
                    for(PkPlanModel pkPlanModel :
                            planModel) {
                        StringBuffer betStrBuf = new StringBuffer();
                        for (int i = 0; i < pkPlanModel.getBetNums().length; i++) {
                            if (pkPlanModel.getBetNums()[i].equals(10)) {
                                betStrBuf.append(pkPlanModel.getBetNums()[i]);
                            } else {
                                betStrBuf.append("0"+pkPlanModel.getBetNums()[i]+" ");
                            }
                        }
                        //System.out.println(betStrBuf.toString());
                        String betStr = betStrBuf.toString();
                        BetUtil betUtil = new BetUtil("-",10);
                        String buyCodeFront = betUtil.buyCodeFront(betStr, pkPlanModel.getRanking()-1);
                        betUtil = new BetUtil("", 10);
                        String buyCode = betUtil.buyCode(betStr.replace(" ", ""), pkPlanModel.getRanking()-1);
                        int buyDouble = LotteryConfig.baseAmount * LotteryConfig.powers[LotteryConfig.pkLoseCount[pkPlanModel.getRanking()]];
                        Bet bet = new Bet(playId,buyCode,buyCodeFront,200,buyDouble);
                        betList.add(bet);
                    }
                    return betList;
                }
            });
            LotteryConfig.allowPk2fBet = false;//设置为不允许下注模式
        }
    }






}
