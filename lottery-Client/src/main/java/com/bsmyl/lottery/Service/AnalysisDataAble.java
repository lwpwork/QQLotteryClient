package com.bsmyl.lottery.Service;

import com.bsmyl.lottery.model.Bet;
import com.bsmyl.lottery.model.PkPlanModel;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface AnalysisDataAble<T> {


    //合格是解析数据用的
    default T dataAnalysis(String data) {
        return null;
    }

    default List<Bet> betAnalysis(List<T> planModel) {
        return null;
    }


}
