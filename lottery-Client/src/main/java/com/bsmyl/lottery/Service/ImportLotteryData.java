package com.bsmyl.lottery.Service;

import java.util.List;

/**
 * describe:
 *
 * @author LiuWenPing
 * @date 2019/02/18
 */
public interface ImportLotteryData<T> {


    List<T> importData(String fileName, AnalysisDataAble analysisDataAble);


}
