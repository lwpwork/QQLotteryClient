package com.bsmyl.lottery.Service;

import com.bsmyl.lottery.model.BetPlanModel;
import com.bsmyl.lottery.model.EnuPosition;
import com.bsmyl.lottery.model.Lottery;

import java.util.List;

public interface BetPlan {

    List<BetPlanModel> genBetPlans(List<Lottery> lotteries);

    List<BetPlanModel> getContinuousPlan(List<Lottery> lotteries);
}
