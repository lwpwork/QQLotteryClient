package com.bsmyl.lottery.feign;


import com.bsmyl.lottery.model.Order;
import com.bsmyl.lottery.model.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@Component
@FeignClient(value = "lottery", url = "${application.cnode.url}")
@RequestMapping("/lottery-wapi/wapi")
public interface LotteryFeignClient {


    @RequestMapping(value = "/LotteryHistoryOpenInfo",method = RequestMethod.POST,consumes = "application/x-www-form-urlencoded")
    public Result LotteryHistoryOpenInfo(@RequestBody String json);


    @RequestMapping(value = "/LotteryUserBet",method = RequestMethod.POST,consumes = "application/json;charset=UTF-8")
    public Result LotteryUserBet(@RequestBody Order order);

    //https://ocsapi-aka.zcgsha.com/cloud/api.do?pa=captcha.next&key=66954091



}
