package com.bsmyl.lottery.feign;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Map;

@Component
@FeignClient(value = "lotteryLoginClient", url = "${application.login.url}")
@RequestMapping("/cloud")
public interface LotteryLoginClient {


    @RequestMapping(value = "/api.do",method = RequestMethod.GET)
    public byte[] getCode(@RequestParam("pa") String pa, @RequestParam("key") String key);


    @RequestMapping(value = "/api/auth.trial.login",method = RequestMethod.POST,consumes = "application/x-www-form-urlencoded")
    public String login(@RequestBody String json);



}
