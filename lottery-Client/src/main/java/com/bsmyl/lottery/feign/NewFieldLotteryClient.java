package com.bsmyl.lottery.feign;

import com.bsmyl.lottery.config.FeignConfiguration;
import com.bsmyl.lottery.model.Result;
import com.bsmyl.lottery.newfield.QqLotteryVO;
import feign.Headers;
import feign.Param;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@Component
@FeignClient(value = "newFieldLottery", url = "${application.newcnod.url}",configuration = FeignConfiguration.class)
@RequestMapping("/api")
public interface NewFieldLotteryClient {


    @RequestMapping(value = "/game-lottery/add-order",method = RequestMethod.POST)
    public Map<String ,Object> addOrder(@RequestParam("text") String text);

    @RequestMapping(value = "/web-ajax/loop-game-lottery",method = RequestMethod.POST)
    public Map<String ,Object> loopGameLottery(@RequestParam("lottery") String lottery);




}
