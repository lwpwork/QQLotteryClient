package com.bsmyl.lottery.model;

public enum EnuPosition {


    DA(5,6,7,8,9),
    XIAO(0,1,2,3,4),
    DAN(1, 3, 5, 7, 9),
    SHUANG(0, 2, 4, 6, 8);

    private final int[] betNum;

    EnuPosition(int... betNum) {
        this.betNum = betNum;
    }

    public int[] betNum(){
        return betNum;
    }


    public static EnuPosition position(char position){
        if (position == '大') {
            return XIAO;
        } else if (position == '小') {
            return DA;
        } else if (position == '单') {
            return SHUANG;
        } else if (position == '双') {
            return DAN;
        }
        return null;
    }


}
