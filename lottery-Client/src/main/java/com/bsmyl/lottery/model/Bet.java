package com.bsmyl.lottery.model;

/**
 * describe:
 *
 * @author LiuWenPing
 * @date 2019/02/14
 */
public class Bet {
    private Long playId;            //玩家ID；
    private String buyCode;         //下注code：56789,,,,
    private String buyCodeFront;   //下注front:5 6 7 8 9,-,-,-,-
    private int singleMoney;      //每注金额：200=2块
    private int buyDouble;         //购买注数
    private int moneyUnit = 3;
    private int rebateRate = 0;
    private int imp = 0;
    private int winningToStop = 1;



    public Bet() {
    }

    public Bet(Long playId, String buyCode, String buyCodeFront, int singleMoney, int buyDouble) {
        this.playId = playId;
        this.buyCode = buyCode;
        this.buyCodeFront = buyCodeFront;
        this.singleMoney = singleMoney;
        this.buyDouble = buyDouble;
    }

    public Long getPlayId() {
        return playId;
    }

    public void setPlayId(Long playId) {
        this.playId = playId;
    }

    public String getBuyCode() {
        return buyCode;
    }

    public void setBuyCode(String buyCode) {
        this.buyCode = buyCode;
    }

    public String getBuyCodeFront() {
        return buyCodeFront;
    }

    public void setBuyCodeFront(String buyCodeFront) {
        this.buyCodeFront = buyCodeFront;
    }

    public int getSingleMoney() {
        return singleMoney;
    }

    public void setSingleMoney(int singleMoney) {
        this.singleMoney = singleMoney;
    }

    public int getBuyDouble() {
        return buyDouble;
    }

    public void setBuyDouble(int buyDouble) {
        this.buyDouble = buyDouble;
    }

    public int getMoneyUnit() {
        return moneyUnit;
    }

    public void setMoneyUnit(int moneyUnit) {
        this.moneyUnit = moneyUnit;
    }

    public int getRebateRate() {
        return rebateRate;
    }

    public void setRebateRate(int rebateRate) {
        this.rebateRate = rebateRate;
    }

    public int getImp() {
        return imp;
    }

    public void setImp(int imp) {
        this.imp = imp;
    }

    public int getWinningToStop() {
        return winningToStop;
    }

    public void setWinningToStop(int winningToStop) {
        this.winningToStop = winningToStop;
    }

    public static void main(String[] args) {
        StringBuffer stringBuffer = new StringBuffer(",,,,,");
        int i = 4;
        stringBuffer.replace(i, i+1, "12345");
        System.out.println(stringBuffer.toString());
        System.out.println("a b c".replace(" ", ""));
    }
}
