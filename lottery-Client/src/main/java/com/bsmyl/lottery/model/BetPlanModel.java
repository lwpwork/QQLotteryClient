package com.bsmyl.lottery.model;

/**
 * describe:
 *
 * @author LiuWenPing
 * @date 2019/02/14
 */
public class BetPlanModel {

    private int [] bOrS;//大或小

    private int [] sOrD;//单或双

    //private int [] betPlan;

    public int[] getbOrS() {
        return bOrS;
    }

    public void setbOrS(EnuPosition bOrS) {
        this.bOrS = bOrS.betNum();
    }

    public int[] getsOrD() {
        return sOrD;
    }

    public void setsOrD(EnuPosition sOrD) {
        this.sOrD = sOrD.betNum();
    }

    /*public int[] getBetPlan() {

        if (bOrS.length > 0 && sOrD.length > 0) {
            ok:for (int i = 0; i < 5; i++) {
                for (int j = 0; j < 5; j++) {
                    if (bOrS[i] == sOrD[j]) {

                    }
                    if (bOrS[i] < sOrD[j]) {

                    }
                }
            }
        }
        return null;
    }*/
}
