package com.bsmyl.lottery.model;

import java.util.Arrays;
import java.util.Date;

/**
 * describe:
 *
 * @author LiuWenPing
 * @date 2019/02/21
 */
public class PkPlanModel {

    private Long id;

    private Long issue;//期号

    private Long lotteryId;

    private Integer [] betNums ;//押注号码

    private Date createTime; //生成时间

    private int ranking;//第几名

    private String planNum;

    private boolean winPlan;



    public PkPlanModel(Integer[] betNums) {
        this.betNums = betNums;
    }

    public PkPlanModel(Long issue, Integer[] betNums, Date createTime, int ranking) {
        this.issue = issue;
        this.betNums = betNums;
        this.createTime = createTime;
        this.ranking = ranking;
        this.planNum = Arrays.toString(betNums);
    }

    public PkPlanModel(Long issue, Long lotteryId, Integer[] betNums, Date createTime, int ranking) {
        this.issue = issue;
        this.lotteryId = lotteryId;
        this.betNums = betNums;
        this.createTime = createTime;
        this.ranking = ranking;
        this.planNum = Arrays.toString(betNums);
    }

    public PkPlanModel() {
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getLotteryId() {
        return lotteryId;
    }

    public void setLotteryId(Long lotteryId) {
        this.lotteryId = lotteryId;
    }

    public String getPlanNum() {
        return planNum;
    }

    public void setPlanNum(String planNum) {
        this.planNum = planNum;
    }

    public Integer[] getBetNums() {
        return betNums;
    }

    public void setBetNums(Integer[] betNums) {
        this.betNums = betNums;
    }

    public Long getIssue() {
        return issue;
    }

    public void setIssue(Long issue) {
        this.issue = issue;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public int getRanking() {
        return ranking;
    }

    public void setRanking(int ranking) {
        this.ranking = ranking;
    }

    public boolean isWinPlan() {
        return winPlan;
    }

    public void setWinPlan(boolean winPlan) {
        this.winPlan = winPlan;
    }
}
