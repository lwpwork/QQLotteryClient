package com.bsmyl.lottery.model;

import java.util.Date;

/**
 * describe:
 *
 * @author LiuWenPing
 * @date 2019/02/21
 */
public class PkDetail {

    private Long id;
    private Long issue;
    private Long lotteryId;
    private Integer type;
    private Integer ariseCount;
    private Integer num;
    private Date lotteryTime;

    public PkDetail(Long lotteryId, Integer type, Integer num, Date lotteryTime) {
        this.lotteryId = lotteryId;
        this.type = type;
        this.num = num;
        this.lotteryTime = lotteryTime;
    }

    public PkDetail(Long issue, Long lotteryId, Integer type, Integer num, Date lotteryTime) {
        this.issue = issue;
        this.lotteryId = lotteryId;
        this.type = type;
        this.num = num;
        this.lotteryTime = lotteryTime;
    }

    public PkDetail(Long issue, Long lotteryId, Integer type, Integer ariseCount, Integer num, Date lotteryTime) {
        this.issue = issue;
        this.lotteryId = lotteryId;
        this.type = type;
        this.ariseCount = ariseCount;
        this.num = num;
        this.lotteryTime = lotteryTime;
    }

    public PkDetail() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIssue() {
        return issue;
    }

    public void setIssue(Long issue) {
        this.issue = issue;
    }

    public Long getLotteryId() {
        return lotteryId;
    }

    public void setLotteryId(Long lotteryId) {
        this.lotteryId = lotteryId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getAriseCount() {
        return ariseCount;
    }

    public void setAriseCount(Integer ariseCount) {
        this.ariseCount = ariseCount;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Date getLotteryTime() {
        return lotteryTime;
    }

    public void setLotteryTime(Date lotteryTime) {
        this.lotteryTime = lotteryTime;
    }
}
