package com.bsmyl.lottery.model;

/**
 * describe:
 *
 * @author LiuWenPing
 * @date 2019/02/15
 */
public class LotteryInfoParam {
    private String cmd = "LotteryHistoryOpenInfo";
    private String channel = "bg";
    private String version = "1.0";
    private String identity = "5019414126636883911917";
    private String sn = "DEMO";
    private String token;

    private Integer lotteryId = 1025;
    private Integer issueNum = 2;
    private Integer clientType = 1;

    public LotteryInfoParam(String token) {
        this.token = token;
    }

    public LotteryInfoParam(String token, Integer lotteryId, Integer issueNum) {
        this.token = token;
        this.lotteryId = lotteryId;
        this.issueNum = issueNum;
    }

    public LotteryInfoParam() {
    }

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getIdentity() {
        return identity;
    }

    public void setIdentity(String identity) {
        this.identity = identity;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getLotteryId() {
        return lotteryId;
    }

    public void setLotteryId(Integer lotteryId) {
        this.lotteryId = lotteryId;
    }

    public Integer getIssueNum() {
        return issueNum;
    }

    public void setIssueNum(Integer issueNum) {
        this.issueNum = issueNum;
    }

    public Integer getClientType() {
        return clientType;
    }

    public void setClientType(Integer clientType) {
        this.clientType = clientType;
    }
}
