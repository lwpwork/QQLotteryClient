package com.bsmyl.lottery.model;

import java.util.Arrays;

/**
 * describe:
 *
 * @author LiuWenPing
 * @date 2019/02/14
 */
public class Lottery {

    private Long id ; //主键id
    private Long lotteryId;  //彩票ID
    private Long issue;  //期号
    private String openNum;  //开奖号码
    private String openTime;   //开奖时间
    private String[] dataExt; //其他开奖

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getLotteryId() {
        return lotteryId;
    }

    public void setLotteryId(Long lotteryId) {
        this.lotteryId = lotteryId;
    }

    public Long getIssue() {
        return issue;
    }

    public void setIssue(Long issue) {
        this.issue = issue;
    }

    public String getOpenNum() {
        return openNum;
    }

    public void setOpenNum(String openNum) {
        this.openNum = openNum;
    }

    public String getOpenTime() {
        return openTime;
    }

    public void setOpenTime(String openTime) {
        this.openTime = openTime;
    }

    public String[] getDataExt() {
        return dataExt;
    }

    public void setDataExt(String[] dataExt) {
        this.dataExt = dataExt;
    }

    @Override
    public String toString() {
        return "Lottery{" +
                "id=" + id +
                ", lotteryId=" + lotteryId +
                ", issue=" + issue +
                ", openNum='" + openNum + '\'' +
                ", openTime='" + openTime + '\'' +
                ", dataExt=" + Arrays.toString(dataExt) +
                '}';
    }
}
