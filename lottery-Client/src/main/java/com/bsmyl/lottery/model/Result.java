package com.bsmyl.lottery.model;

/**
 * describe:
 *
 * @author LiuWenPing
 * @date 2019/02/14
 */
public class Result {
    private int code;
    private String desc;
    private String reason;
    private String identity;
    private String version;
    private String serviceTime;
    private OpenData data;
    private Long serviceTimeZone;


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getIdentity() {
        return identity;
    }

    public void setIdentity(String identity) {
        this.identity = identity;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getServiceTime() {
        return serviceTime;
    }

    public void setServiceTime(String serviceTime) {
        this.serviceTime = serviceTime;
    }

    public OpenData getData() {
        return data;
    }

    public void setData(OpenData data) {
        this.data = data;
    }

    public Long getServiceTimeZone() {
        return serviceTimeZone;
    }

    public void setServiceTimeZone(Long serviceTimeZone) {
        this.serviceTimeZone = serviceTimeZone;
    }
}
