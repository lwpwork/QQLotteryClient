package com.bsmyl.lottery.model;

import java.util.List;

/**
 * describe:
 *
 * @author LiuWenPing
 * @date 2019/02/14
 */
public class OpenData {

    private List<String> position ;

    private List<Lottery> historyList;

    public List<String> getPosition() {
        return position;
    }

    public void setPosition(List<String> position) {
        this.position = position;
    }

    public List<Lottery> getHistoryList() {
        return historyList;
    }

    public void setHistoryList(List<Lottery> historyList) {
        this.historyList = historyList;
    }
}
