package com.bsmyl.lottery.model;

/**
 * describe:
 *
 * @author LiuWenPing
 * @date 2019/02/14
 */
public class Order {
    private String cmd;
    private int lotteryId;
    private String issue;
    private int winningToStop = 1;
    private String betList;
    private int clientType = 1;
    private String channel = "bg";
    private String version = "1.0";
    private String identity ;
    private String sn = "DEMO" ;
    private String token;


    public Order(String cmd, int lotteryId, String issue, String betList, String identity, String token) {
        this.cmd = cmd;
        this.lotteryId = lotteryId;
        this.issue = issue;
        this.betList = betList;
        this.identity = identity;
        this.token = token;
    }

    public Order() {
    }

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public int getLotteryId() {
        return lotteryId;
    }

    public void setLotteryId(int lotteryId) {
        this.lotteryId = lotteryId;
    }

    public String getIssue() {
        return issue;
    }

    public void setIssue(String issue) {
        this.issue = issue;
    }

    public int getWinningToStop() {
        return winningToStop;
    }

    public void setWinningToStop(int winningToStop) {
        this.winningToStop = winningToStop;
    }

    public String getBetList() {
        return betList;
    }

    public void setBetList(String betList) {
        this.betList = betList;
    }

    public int getClientType() {
        return clientType;
    }

    public void setClientType(int clientType) {
        this.clientType = clientType;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getIdentity() {
        return identity;
    }

    public void setIdentity(String identity) {
        this.identity = identity;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
