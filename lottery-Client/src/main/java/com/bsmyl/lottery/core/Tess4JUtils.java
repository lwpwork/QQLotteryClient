package com.bsmyl.lottery.core;

import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

import java.io.*;

/**
 * describe:
 *
 * @author LiuWenPing
 * @date 2019/03/05
 */
public class Tess4JUtils {


    public static String discernCode(byte [] bytes){
        String resultCode = null;
        Tess4JUtils tess4JUtils = new Tess4JUtils();
        System.out.println("======================================================================================" + (tess4JUtils.getPath() + "/static/safeCode (1).png").replace("!", "").replace("file:/", ""));
        System.out.println("======================================================================================"+(tess4JUtils.getPath()+"/tessdata").replace("!","").replace("file:/", ""));
        File file = new File((tess4JUtils.getPath()+"static/safeCode (1).png").replace("!","").replace("file:/", ""));
        OutputStream output = null;
        try {
            output = new FileOutputStream(file);
            BufferedOutputStream bufferedOutput = new BufferedOutputStream(output);
            bufferedOutput.write(bytes);
            ITesseract instance = new Tesseract();
            instance.setDatapath((tess4JUtils.getPath()+"tessdata").replace("!","").replace("file:/", ""));
            //long startTime = System.currentTimeMillis();
            resultCode = instance.doOCR(file).replaceAll("&","");
        } catch (TesseractException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return resultCode;
    }

    public String getPath(){
        return getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
    }
    public static void main(String[] args) {
        Tess4JUtils tess4JUtils = new Tess4JUtils();
        System.out.println(System.getProperty("user.dir"));
    }


}
