package com.bsmyl.lottery.core;

import java.util.Comparator;
import java.util.Map;

/**
 * describe:
 *
 * @author LiuWenPing
 * @date 2019/02/22
 */
public class MapValueComparator implements Comparator<Map.Entry<String, String>> {

    @Override
    public int compare(Map.Entry<String, String> me1, Map.Entry<String, String> me2) {
        return me1.getValue().compareTo(me2.getValue());
    }
}
