package com.bsmyl.lottery.core;

import javax.swing.*;
import java.text.SimpleDateFormat;

/**
 * describe:
 *
 * @author LiuWenPing
 * @date 2019/02/25
 */
public class BetUtil {

    private StringBuffer pattern;

    public BetUtil(String placeholder,int size) {
        StringBuffer pattern = new StringBuffer();
        for (int i = 0; i < size; i++) {
            if (placeholder != null && !placeholder.equals("")) {
                pattern.append(placeholder);
                if (i < size - 1) {
                    pattern.append(",");
                }
            } else {
                pattern.append(",");
            }
        }
        this.pattern = pattern;
    }

    public String buyCodeFront(String bet,int index){
        pattern.replace(2*index, 2 * index+1, bet);
        return pattern.toString();
    }


    public String buyCode(String bet,int index){
        pattern.replace(index, index+1, bet);
        return pattern.toString();
    }


    public static void main(String[] args) {
        String placeholder="";
        int size=10;
        StringBuffer pattern = new StringBuffer();
        for (int i = 0; i < size; i++) {
            if (placeholder != null && !placeholder.equals("")) {
                pattern.append(placeholder);
                if (i < size - 1) {
                    pattern.append(",");
                }
            } else {
                pattern.append(",");
            }
        }
        System.out.println(pattern.toString());
    }


}
