package com.bsmyl.lottery.core;

import com.alibaba.fastjson.JSON;
import com.bsmyl.lottery.model.PkPlanModel;

import javax.validation.constraints.Max;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * describe:
 *
 * @author LiuWenPing
 * @date 2019/04/29
 */
public class SimulationPlanConfig {
    public static Integer gameNums = 0;//玩游戏局数

    public static Integer initScore = 0;//玩家初始化金额

    public static Integer strategyCount = 0;//策略数

    public static Integer wallet; //玩家钱包金额

    public static Integer singleScore; //玩家单笔金额: 初始化金额/策略数 = 玩家单笔金额

    public static Integer gameIndex = 0;//游戏当前局数

    public static Integer wrongCount = 0;

    public static Boolean isBet = false; //是否已下注

    public static Boolean isStart = false;//是否开始游戏

    public static Boolean isAllowBet = false;// 是否允许下注.(检测是否达到策略允许下注的临界点.)

    public static double winRate = 50;

    public static Integer bet;

    public static Map<String,Object> getMap(){
        Map<String, Object> map = new HashMap<>();
        map.put("gameNums", gameNums);
        map.put("initScore", initScore);
        map.put("strategyCount", strategyCount);
        map.put("wallet", wallet);
        map.put("singleScore", singleScore);
        map.put("gameIndex", gameIndex);
        map.put("wrongCount", wrongCount);
        map.put("isBet", isBet);
        map.put("isStart", isStart);
        map.put("isAllowBet", isAllowBet);
        return map;
    }

    public static void reInitSingleScore(){
        Integer score = initScore / strategyCount;
        if (score <= wallet) {
            singleScore = score;
        } else {
            singleScore = 0;
            isStart = false;
        }
        gameIndex = 0;
        wrongCount = 0;
    }

    //押注倍率。
    public static int[] powers = new int[]{1,2,4,8,16,32,64,128,256,512,1024};

    public static void init(Integer initScore,Integer strategyCount,Integer gameNums){
        SimulationPlanConfig.initScore = initScore;
        SimulationPlanConfig.wallet = initScore;
        SimulationPlanConfig.gameNums = gameNums;
        SimulationPlanConfig.singleScore = initScore / strategyCount;
        SimulationPlanConfig.strategyCount = strategyCount;
        SimulationPlanConfig.wrongCount = 0;
        SimulationPlanConfig.gameIndex = 0;
    }

    public static void bet(){
        Integer bet = getBet();
        singleScore -= bet;
        wallet -= bet;
        isBet = true;

        SimulationPlanConfig.bet = bet ;
    }


    public static Integer getBet(){
        Integer should = (initScore / strategyCount/117)<<wrongCount;
        if (should > singleScore) {
            return singleScore;
        } else {
            return should;
        }
    }



    public static void main(String[] args) throws IllegalAccessException, InstantiationException {
        //PkPlanModel pkPlanModel = new PkPlanModel();
        /**
         * Java反射的学习 :
         * Class是一个泛型类。
         */
        //获取class方式1
        //Class<PkPlanModel> planClass = PkPlanModel.class;
        //获取class方式2
        //Class planClass = pkPlanModel.getClass();
        //获取class方式3
        //Class planClass = Class c3=Class.forName("com.bsmyl.lottery.model.PkPlanModel");

        //PkPlanModel pkPlanModel = planClass.newInstance();

        System.out.println(test4());
    }

    public static int test4() {
        int b = 20;

        try {
            System.out.println("try block");

            b = b / 0;

            return b += 80;
        } catch (Exception e) {

            b += 15;
            System.out.println("catch block");
        } finally {

            System.out.println("finally block");

            if (b > 25) {
                System.out.println("b>25, b = " + b);
            }

            b += 50;
        }

        return 204;
    }

    public static Map<Integer,List<String>> group(List<String> param){
        Map<Integer, List<String>> groups = new HashMap<>();
        for(String s :
                param) {
            char[] chars = s.toCharArray();
            Integer groupBy = 1;
            for (int i = 0; i < s.length(); i++) {
                groupBy *= (int)chars[i];
            }
            List<String> group = groups.get(groupBy);
            if (group == null) {
                group = new ArrayList<>();
                group.add(s);
                groups.put(groupBy, group);
            } else {
                group.add(s);
            }
        }
        return groups;
    }

}
