package com.bsmyl.lottery.core;

import com.alibaba.fastjson.JSON;
import com.bsmyl.lottery.newfield.QqLotteryConfig;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.*;

/**
 * describe:
 * 提供一些Map操作的工具
 *
 * @author LiuWenPing
 * @date 2019/02/22
 */
public class MapUtile {

    /**
     * 使用 Map按value进行排序
     *
     * @param oriMap
     * @return
     */
    public static Map<String, Object> sortMapByValue(Map<String, Object> oriMap, Comparator<Map.Entry<String, Object>> comparator) {
        if (oriMap == null || oriMap.isEmpty()) {
            return null;
        }
        Map<String, Object> sortedMap = new LinkedHashMap<String, Object>();
        List<Map.Entry<String, Object>> entryList = new ArrayList<Map.Entry<String, Object>>(oriMap.entrySet());
        Collections.sort(entryList, comparator);

        Iterator<Map.Entry<String, Object>> iter = entryList.iterator();
        Map.Entry<String, Object> tmpEntry = null;
        while (iter.hasNext()) {
            tmpEntry = iter.next();
            sortedMap.put(tmpEntry.getKey(), tmpEntry.getValue());
        }
        return sortedMap;
    }


    /**
     * Map转对象
     * @param map
     * @param beanClass
     * @return
     * @throws Exception
     */
    public static Object mapToObject(Map<String, Object> map, Class<?> beanClass) throws Exception {
        if (map == null)
            return null;

        Object obj = beanClass.newInstance();
        BeanInfo beanInfo = Introspector.getBeanInfo(obj.getClass());
        PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
        for (PropertyDescriptor property : propertyDescriptors) {
            Method setter = property.getWriteMethod();
            if (setter != null) {
                setter.invoke(obj, map.get(property.getName()));
            }
        }
        return obj;
    }

    /**
     * 对象转Map集合
     *
     * @param obj
     * @return
     * @throws Exception
     */
    public static Map<String, Object> objectToMap(Object obj) throws Exception {
        if (obj == null)
            return null;
        Map<String, Object> map = new HashMap<String, Object>();
        BeanInfo beanInfo = Introspector.getBeanInfo(obj.getClass());
        PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
        for (PropertyDescriptor property : propertyDescriptors) {
            String key = property.getName();
            if (key.compareToIgnoreCase("class") == 0) {
                continue;
            }
            Method getter = property.getReadMethod();
            Object value = getter != null ? getter.invoke(obj) : null;
            map.put(key, value);
        }
        return map;
    }


    public static void main(String[] args) throws Exception {

        Map<String, Object> map = new HashMap<>();
        QqLotteryConfig qqLotteryConfig = new QqLotteryConfig();
        qqLotteryConfig.setDw("1");
        qqLotteryConfig.setLhbg("2");
        qqLotteryConfig.setMultiple(5);

        map = MapUtile.objectToMap(qqLotteryConfig);
        System.out.println(JSON.toJSONString(map));

    }


}
