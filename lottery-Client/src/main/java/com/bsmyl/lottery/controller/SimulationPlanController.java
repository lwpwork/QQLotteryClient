package com.bsmyl.lottery.controller;

import com.bsmyl.lottery.core.SimulationPlanConfig;
import com.bsmyl.lottery.scor.Result;
import com.bsmyl.lottery.scor.ResultGenerator;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * describe:
 *
 * @author LiuWenPing
 * @date 2019/05/13
 */
@Controller
@RequestMapping("/simulationPlan")
public class SimulationPlanController {


    /**
     * 初始化模拟下注.
     *
     * @param initScore 玩家初始化金额
     * @param strategyCount 策略数
     * @param gameNums  玩游戏局数
     * @return
     */
    @RequestMapping("/initSimulationPlan")
    @ResponseBody
    public Result initSimulationPlan(Integer initScore,Integer strategyCount,Integer gameNums){
        SimulationPlanConfig.init(initScore,strategyCount,gameNums);
        return ResultGenerator.genSuccessResult();
    }

    @RequestMapping("/startSimulationPlan")
    @ResponseBody
    public Result startSimulationPlan(){
        SimulationPlanConfig.isStart = true;
        //SimulationPlanConfig.isAllowBet = true;
        return ResultGenerator.genSuccessResult();
    }

    /**
     * 获取模拟下注详细信息
     * @return
     */
    @RequestMapping("/getSimulationPlan")
    @ResponseBody
    public Result getSimulationPlan(){
        Map<String, Object> map = SimulationPlanConfig.getMap();
        return ResultGenerator.genSuccessResult(map);
    }

}
