package com.bsmyl.lottery.controller;

import cn.hutool.core.date.DateUtil;
import com.bsmyl.lottery.Service.impl.IssueServiceImpl;
import com.bsmyl.lottery.config.LotteryConfig;
import com.bsmyl.lottery.dao.LotteryDao;
import com.bsmyl.lottery.model.Lottery;
import com.bsmyl.lottery.model.QQLottery;
import com.bsmyl.lottery.scor.Result;
import com.bsmyl.lottery.scor.ResultGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.mail.MailProperties;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * describe:
 *
 * @author LiuWenPing
 * @date 2019/02/15
 */

@Controller
@RequestMapping("/lottery")
public class LotteryController {

    //DEMO0500711015C35AA4CF0BEB23d6c6
    @RequestMapping("/start")
    @ResponseBody
    public Result start(String token) {
        if (token == null) {
            return ResultGenerator.genFailResult("请输入购买彩票用户Token");
        }
        LotteryConfig.Token = token;
        return ResultGenerator.genSuccessMsgResult("彩票自动押注已开启");
    }

    @RequestMapping("/stop")
    @ResponseBody
    public Result stop(){
        LotteryConfig.Token = null;
        return ResultGenerator.genSuccessMsgResult("彩票自动押注已关闭");
    }
    @RequestMapping("/pk2fenLogin")
    @ResponseBody
    public Result pk2fenLogin(String token){
        if (token == null) {
            return ResultGenerator.genFailResult("请输入购买彩票用户Token");
        }
        IssueServiceImpl.token = token;
        return ResultGenerator.genSuccessMsgResult("彩票数据开始导入，并在特定时间开始生成计划。");

    }

    @RequestMapping("/pk2fenStartBet")
    @ResponseBody
    public Result pk2fenStartBet(){
        LotteryConfig.pk10StartBet = true;
        return ResultGenerator.genSuccessMsgResult("2分pk10开启自动押注。");
    }

    @RequestMapping("/pk2fenStopBet")
    @ResponseBody
    public Result pk2fenStopBet(){
        LotteryConfig.pk10StartBet = false;
        return ResultGenerator.genSuccessMsgResult("2分pk10关闭自动押注。");
    }

    @Autowired
    private LotteryDao lotteryDao;


    @RequestMapping("/clearLotteryData")
    @ResponseBody
    public Result clearPkData(){
        lotteryDao.delete();
        return ResultGenerator.genSuccessMsgResult("2分pk10清除数据");
    }






    public static void main(String[] args) throws ParseException {
        String str = "2019/2/17 2:51\t\t\t2\t8\t7\t8\t3\t28783";
        System.out.println(str);
        //开奖时间    万   千   百   十   个   开奖号码
    }




}
