package com.bsmyl.lottery.controller;

import com.bsmyl.lottery.dao.LotteryDao;
import com.bsmyl.lottery.dao.QQLotteryDao;
import com.bsmyl.lottery.model.Lottery;
import com.bsmyl.lottery.scor.Result;
import com.bsmyl.lottery.scor.ResultGenerator;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * describe:
 *
 * @author LiuWenPing
 * @date 2019/02/26
 */
@Controller
@RequestMapping("/TencentLotteryController")
public class TencentLotteryController {


    @Autowired
    private LotteryDao lotteryDao;

    @Autowired
    private QQLotteryDao qqLotteryDao;


    /**
     * 测试分页查询
     * @param pageNum   页码
     * @param pageSize  每页数据条数
     * @return
     */
    @ResponseBody
    @RequestMapping("/testPageSelect")
    public Result testPageSelect(int pageNum,int pageSize){
        //启动分页插件
        PageHelper.startPage(pageNum, pageSize);
        //查询列表。
        List<Lottery> lotteries = lotteryDao.lotteryListPage(1404L);
        //分页。
        PageInfo<Lottery> pageInfo = new PageInfo<>(lotteries);
        return ResultGenerator.genSuccessMsgResult(pageInfo, "查询分页数据");
    }


    /**
     *  龙虎斗统计
     * @param pageNum
     * @param pageSize
     * @param type01    five - four type01=5 , type02 = 4
     * @param type02
     * @param groupType  1-每周数据；2-每月数据；3-每年数据；default-每天数据
     * @return
     */
    @ResponseBody
    @RequestMapping("/theChineseBoxer")
    public Result theChineseBoxer(int pageNum, int pageSize,int type01,int type02, Integer groupType) {
        String groupTypeStr = getTypeStr(groupType);
        PageHelper.startPage(pageNum, pageSize);
        List<Map<String, Object>> theChineseBoxerList = qqLotteryDao.theChineseBoxerList(type01,type02,groupTypeStr);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(theChineseBoxerList);
        return ResultGenerator.genSuccessMsgResult(pageInfo,"龙虎统计");
    }


    /**
     * 前中后统计
     * @param pageNum
     * @param pageSize
     * @param groupType
     * @return
     */
    @ResponseBody
    @RequestMapping("/inTheFormer")
    public Result inTheFormer(int pageNum, int pageSize, int groupType){
        String groupTypeStr = getTypeStr(groupType);
        PageHelper.startPage(pageNum, pageSize);
        List<Map<String,Object>> inTheFormerList = qqLotteryDao.inTheFormerList(groupTypeStr);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(inTheFormerList);
        return ResultGenerator.genSuccessMsgResult(pageInfo, "前中后统计");
    }


    /**
     * 大小单双统计
     * @param pageNum
     * @param pageSize
     * @param groupType
     * @return
     */
    @ResponseBody
    @RequestMapping("/daXiaoDanShuang")
    public Result daXiaoDanShuang(int pageNum, int pageSize, int groupType){
        String groupTypeStr = getTypeStr(groupType);
        PageHelper.startPage(pageNum, pageSize);
        List<Map<String,Object>> daXiaoDanShuang = qqLotteryDao.daXiaoDanShuang(groupTypeStr);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(daXiaoDanShuang);
        return ResultGenerator.genSuccessMsgResult(pageInfo, "大小单双统计");
    }

    /**
     * 连赢中断排序
     * @param ariseCount
     * @return
     */
    @ResponseBody
    @RequestMapping("/serialNumber")
    public Result serialNumber(int ariseCount){
        String ranking = null;
        int winCount = 0;
        int betCount = 0;
        for (int i = 0; i < 5; i++) {
            switch (i) {
                case 0:
                    ranking = "1";
                    break;
                case 1:
                    ranking = "2";
                    break;
                case 2:
                    ranking = "3";
                    break;
                case 3:
                    ranking = "4";
                    break;
                case 4:
                    ranking = "5";
                    break;
            }
            Map<String,Object> daXiao = qqLotteryDao.daxiaoSerialNumber(ariseCount, ranking);
            Map<String,Object> danshuang = qqLotteryDao.danShuangSerialNumber(ariseCount, ranking);
            winCount += (Integer.valueOf(daXiao.get("winCount").toString()) + Integer.valueOf(danshuang.get("winCount").toString()));
            betCount += (Integer.valueOf(daXiao.get("betCount").toString()) + Integer.valueOf(danshuang.get("betCount").toString()));
        }
        Map<String, Integer> res = new HashMap<>();
        res.put("winCount", winCount);
        res.put("betCount", betCount);

        return ResultGenerator.genSuccessMsgResult(res, "连赢中断统计");
    }

    public String getTypeStr(int groupType) {
        String groupTypeStr = null;
        switch (groupType) {
            case 1:
                groupTypeStr = "%Y%u";
                break;
            case 2:
                groupTypeStr = "%Y%m";
                break;
            case 3:
                groupTypeStr = "%Y";
                break;
            default:
                groupTypeStr = "%Y%m%d";
                break;
        }
        return groupTypeStr;
    }





}
